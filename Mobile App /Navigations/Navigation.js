import React from "react";
import {createAppContainer  ,createSwitchNavigator  } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator , createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Ionicons ,MaterialCommunityIcons } from "@expo/vector-icons";
import HomeScreen from './../Screens/HomeScreen'
import PostsScreen from './../Screens/PostsScreen'
import CameraScreen from "./../Screens/CameraScreen"
import NotificationScreen from "./../Screens/NotificationScreen"
import ProfileScreen from "./../Screens/ProfileScreen"
import WelcomeScreen from './WelcomeScreen'
import LoginScreen from './../Screens/LogIn/LoginScreen'
const Home = createStackNavigator({
  HomeScreen
})
const Posts = createStackNavigator({
  PostsScreen
})
const Camera = createStackNavigator({
  CameraScreen
})
const Notification = createStackNavigator({
  NotificationScreen
})
const Account = createStackNavigator({
  ProfileScreen
})
Home.navigationOptions = {
  tabBarIcon: ( tintColor ) => (
    <MaterialCommunityIcons style={{marginTop:2}} color={ tintColor.focused ? '#f65a22' :'gray' } name="home" size={30} />
  ),
  tabBarLabel:' '
}
Posts.navigationOptions ={
  tabBarIcon:(tintColor)=>(
    <MaterialCommunityIcons style={{marginTop:2}} color={ tintColor.focused ? '#f65a22' :'gray' } name="chat" size={30}  />
  ),
  tabBarLabel:' '
}
Camera.navigationOptions={
  tabBarIcon:(tintColor)=>(
    <MaterialCommunityIcons style={{marginTop:2}}  color={ tintColor.focused ? '#f65a22' :'gray' } name="camera" size={30}  />
  ),
  tabBarLabel:' '
}
Notification.navigationOptions={
  tabBarIcon:(tintColor)=>(
    <Ionicons style={{marginTop:2}}  color={ tintColor.focused ? '#f65a22' :'gray' } name="ios-notifications" size={30}  />
  ),
  tabBarLabel:' '
}
const Login = createStackNavigator({
  LoginScreen
})
const bottomNavigator = createBottomTabNavigator({
  Home,
  Camera,
  Posts,
  Notification
})

const tabNav = createDrawerNavigator({
  Home : bottomNavigator,
  profile: ProfileScreen
},
{
  headerMode: 'screen',
  navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
      swipeEnabled: false,
      drawerLockMode: 'locked-closed'
  }),
  drawerBackgroundColor : 'transparent' ,
  drawerPosition: 'right',
  contentComponent: (props) => <ProfileScreen {...props} />
}
)
const switchNav = createSwitchNavigator({
  WelcomeScreen,
  Login,
  tabNav,
})
export default  createAppContainer(switchNav)