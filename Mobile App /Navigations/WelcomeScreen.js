import React ,{useEffect} from 'react'
import {View , Image, AsyncStorage} from 'react-native'
import { vw, vh} from 'react-native-expo-viewport-units';

export default  props => {
  useEffect(()=>{
    (async()=>{
    const getSavedEmail = await AsyncStorage.getItem('email')
    if(getSavedEmail != null){
      props.navigation.navigate('tabNav')
    }
    else{
      props.navigation.navigate('Login')

    }
    })()
  })
    return (
      <>
        <View style={{flex:1 , backgroundColor:'white', alignItems:'center' , justifyContent:'center' , backgroundColor:'black'}}>
        <Image  style={{width: vw(80), height: vh(40)}} source={{uri: 'https://seeklogo.net/wp-content/uploads/2011/06/orange-logo-vector.png'}}></Image>
        </View>
      </>
    )
  }
