/*  _                          ___         __  __        __ 
   / \   ___  ___ _ __ ___    / _ \  __ _ / _|/ _| __ _ / _|
  / _ \ / __|/ _ \ '_ ` _ \  | | | |/ _` | |_| |_ / _` | |_ 
 / ___ \\__ \  __/ | | | | | | |_| | (_| |  _|  _| (_| |  _|
/_/   \_\___/\___|_| |_| |_|  \__\_\\__,_|_| |_|  \__,_|_|  
*/
import React from 'react';
import Navigation from './Navigations/Navigation'
export default () => <Navigation/>