import React, { useState, useEffect } from 'react'
import { Text, View, Image, FlatList, SafeAreaView } from 'react-native'
import { vw, vh } from 'react-native-expo-viewport-units';
import axios from 'axios'
import Lightbox from 'react-native-lightbox';

export default HomeScreen = () => {
  const [posts, setPosts] = useState([])
  const [load, setLoad] = useState(false)
  const [isFetching, setIsFetching] = useState(false)
  useEffect(() => {
    if (!load) {
      loadData()
    }
  })
  const loadData = () => {
    const url = 'https://orange-coding.herokuapp.com/file/view_all'
    axios.request({
      url 
    }).then(({ data }) => {
      setPosts(data)
      setLoad(true)
      setIsFetching(false)
    })
      .catch(err => {
        console.log(err)
      })
  }

  return (
    <SafeAreaView style={{ flex: 1, alignItems: 'center' }}>
      <FlatList
        data={posts}
        onRefresh={() => loadData()}
        refreshing={isFetching}
        renderItem={({ item }) => <View style={{ flexDirection: 'column', marginBottom: vh(2) }}>
        <View style={{backgroundColor:'lightgray' , width:vw(100) , height:1}}></View>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Image style={{ height: vh(5), width: vw(10), marginRight: 6, marginLeft: 3 }} source={{ uri: 'https://seeklogo.net/wp-content/uploads/2011/06/orange-logo-vector.png' }}></Image>
            <View>
            <Text style={{fontWeight:'bold', marginBottom:1}}>{item.name}</Text>
            <Text style={{ width: vw(86) }} >{item.body}</Text>
            </View>
            <Lightbox>
            <Image style={{ width: vw(99), height: vh(30), marginBottom: 1, marginTop : vh(2), marginLeft:vh(0.25) }} source={{ uri: `https://orange-coding.herokuapp.com/file/view_one?file_name=${item.filename}`,  cache: 'only-if-cached' }}></Image>
            </Lightbox>
          </View>
        </View>}
        keyExtractor={item => item._id.$oid} />
    </SafeAreaView>
  )
}
HomeScreen.navigationOptions = {
  title: 'Orange Coding Life'
}

