import React, { useState , useEffect } from 'react'
import {Text , View , FlatList , SafeAreaView,Image , TouchableOpacity , Alert, AsyncStorage , Platform } from 'react-native'
import { vw, vh } from 'react-native-expo-viewport-units';
import axios from 'axios'
// import Dialog from "react-native-dialog";

export default NotificationScreen =()=>{
  const [posts, setPosts] = useState([])
  const [load, setLoad] = useState(false)
  const [isFetching, setIsFetching] = useState(false)
  useEffect(() => {
    if (!load) {
      loadData()
    }
  })
  const loadData = () => {
    const url = 'https://orange-coding.herokuapp.com/file/view_all'
    axios.request({
      url 
    }).then(async({ data }) => {
      const adminPrivileges = await AsyncStorage.getItem('admin_user')
      const currentName = await AsyncStorage.getItem('name')
      if (adminPrivileges == null){
        const newData = data.filter(post=>{
          return post.name == currentName
        })
        setPosts(newData)
      }else{
        setPosts(data)
      }   
      setLoad(true)
      setIsFetching(false)
    })
      .catch(err => {
        alert(err)
        console.log(err)
      })
  }
  const deleteHandler=(data)=>{
    const file_name = data.filename
    const post_id = data._id.$oid
    if(Platform.OS == 'android'){
     Alert.alert(
      'Delete Post',
      'Are you sure you want to delete this post?', // <- this part is optional, you can pass an empty string
      [
        {text: 'Cancel'},
        {text: 'OK', onPress: () =>   deletePostHandler(file_name,post_id)},
      ],
      {cancelable: true},
    );
    }
    Alert.prompt('Are you sure you want to delete this post?','Please Type DELETE',(event)=>{
      if(event.toLowerCase(event) == 'delete'){
        deletePostHandler(file_name,post_id)
      }
      
    }) 
  }
  const deletePostHandler=(file_name,post_id)=>{
      const url = 'https://orange-coding.herokuapp.com/file/delete_file'
      axios.request({
        url,
        method:'POST',
        data :{file_name}
      }).then(({data})=>{
        console.log(data)
      }).catch(err=>{
        alert(`file not found ${err}`)
      })
      .then(()=>{
        const url = 'https://orange-coding.herokuapp.com/file/delete_file_path_db'
        axios.request({
          url,
          method:'DELETE',
          data : {post_id}
        })
        .then(({data})=>{
          if(data)loadData()
        }).catch(err=>{
          alert(`meta data not found ${err}`)
        })
      })
    
  }
  return (
    <>
    <SafeAreaView style={{ flex: 1, alignItems: 'center' }}>
      <FlatList
        data={posts}
        onRefresh={() => loadData()}
        refreshing={isFetching}
        renderItem={({ item }) =>  <TouchableOpacity onPress={()=>deleteHandler(item)} style={{ flexDirection: 'row', marginBottom: vh(2) , width:vw(100) }}>
        <Image style={{ width: vw(40), height: vh(15), marginBottom: 1,  marginRight: 6, marginLeft: 3 }} source={{ uri: `https://orange-coding.herokuapp.com/file/view_one?file_name=${item.filename}` }}></Image>
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <View style={{justifyContent:'center' }}>
            <Text style={{fontWeight:'bold', marginBottom:1}}>{item.name}</Text>
            <Text >{item.body}</Text>
            </View>
          </View>
        </TouchableOpacity>}
        keyExtractor={item => item._id.$oid} />
    </SafeAreaView>
    </>
  )
}
NotificationScreen.navigationOptions={
  title: 'Notification Screen'
}