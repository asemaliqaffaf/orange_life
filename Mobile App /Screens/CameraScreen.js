import React, { useEffect, useState } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, AsyncStorage, ScrollView } from 'react-native'
import { vw, vh } from 'react-native-expo-viewport-units';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import axios from 'axios'
import moment from 'moment-timezone'
export default CameraScreen = props => {
    const [hasPermission, setHasPermission] = useState()
    const [scanned, setScanned] = useState(false)
    useEffect(() => {
        (async () => {
            const { status } = await Permissions.askAsync(Permissions.CAMERA);
            setHasPermission(status === 'granted')
        })()
    })
    const handleBarCodeScanned = ({ data }) => {
        const LA = moment().tz("America/Los_Angeles").format('MM70HH mmDD98');
        if (LA == data.split('_')[1]) {
            setScanned(true)
        }
    }
    const signIn = async () => {
        const url = 'https://orange-coding.herokuapp.com/att/check_in'
        const name = await AsyncStorage.getItem('name')
        await axios.request({
            url,
            method: 'patch',
            data: {
                name
            }
        }).then(({ data }) => {
            console.log(data)
            setScanned(!scanned)
        })
            .catch(err => {
                console.log(err)
                alert(err)
            })

    }
    const signOut = async () => {
        const url = 'https://orange-coding.herokuapp.com/att/check_out'
        const name = await AsyncStorage.getItem('name')
        await axios.request({
            url,
            method: 'put',
            data: {
                name
            }
        }).then(({ data }) => {
            console.log(data)
            setScanned(!scanned)
        })
            .catch(err => {
                console.log(err)
                alert(err)
            })


    }
    if (hasPermission == null) {
        return <View></View>
    }
    else if (hasPermission == false) {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>No access to camera</Text></View>)
    } else if (scanned) {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'black' }}>
            <Text style={{ color: 'white', fontSize: vw(5), fontWeight: 'bold', alignItems: 'center' }}>Welcome to Orange Coding Academy</Text>
            <TouchableOpacity style={styles.buttonContainerCheckIn} onPress={signIn}>
                <Text style={{ color: 'white', fontSize: 18 }}>Check in</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainerCheckOut} onPress={signOut}>
                <Text style={{ color: 'white', fontSize: 18 }}>Check out</Text>
            </TouchableOpacity>

        </View>)
    }
    else {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <BarCodeScanner
                    style={{ height: vh(100), width: vw(100) }}
                    onBarCodeScanned={handleBarCodeScanned} />
            </View>
        )
    }
}
CameraScreen.navigationOptions = {
    title: 'Check in/out'
}
const styles = StyleSheet.create({
    buttonContainerCheckIn: {
        marginTop: vh(1),
        height: vh(8),
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
        width: "90%",
        borderRadius: 5,
        backgroundColor: "orange"
    },
    buttonContainerCheckOut: {
        marginTop: vh(1),
        height: vh(8),
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
        width: "90%",
        borderRadius: 5,
        backgroundColor: "gray"
    }
})

// const name = await AsyncStorage.getItem('name')
// const FullDate = `${moment().format('dddd')}:${moment().format('DD')}-${moment().format('MM')}-${moment().format('YYYY')}`            // 