import React, { useState, useEffect } from 'react'
import { Text, View, Button, Image, TextInput, StyleSheet, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native'
import axios from 'axios'
import * as ImagePicker from 'expo-image-picker'
import Constants from 'expo-constants'
import { vw, vh } from 'react-native-expo-viewport-units'
import * as Permissions from 'expo-permissions'
import * as Progress from 'react-native-progress';

const TextField = props => {
  return (
    <TextInput
      {...props}
      editable
      maxLength={400}
    />
  )
}
export default postScreen = () => {
  const [image, setImage] = useState()
  const [bodyText, setBodyText] = useState("")
  const [imgType, setImgType] = useState()
  const [type, setType] = useState()
  const [uri, setUri] = useState()
  const [progress , setProgress] = useState()
  useEffect(() => {
    getPermissionAsync()
  })
  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }
  const _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    })
    if (result.uri != null) {
      setImgType(result.uri.split('.')[1])
    }
    setType(result.type)
    setUri(result.uri)

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };
  const submit = async () => {
    setProgress(true);
    const name = await AsyncStorage.getItem('name')
    const url = 'https://orange-coding.herokuapp.com/file/upload'
    const data = new FormData()
    await data.append('image', {
      uri,
      type: `${type}/${imgType}`,
      name: uri
    })
    await axios.request({
      url,
      method: 'POST',
      data,
      params: {
        body: bodyText,
        name
      }
    })
      .then(res => {
        console.log(res.data)
        restart()
      })
      .catch(err => {
        alert(err)
      })
  }
  const restart = () => {
    setImage()
    setBodyText("")
    setImgType()
    setType()
    setUri()
    setProgress()
  }
  return (
    <ScrollView>

      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        {image != null ?
          <Image source={{ uri: image }} style={{ width: vw(100), height: vh(40) }} /> : <View style={{ width: vw(100), height: vh(20) }} ></View>}
        <TextField multiline
          numberOfLines={4} placeholder='what do you think?' placeholderTextColor='gray' style={styles.input} onChangeText={(text) => setBodyText(text)}>{bodyText}</TextField>
        <TouchableOpacity style={styles.buttonContainer} onPress={_pickImage}>
          <Text style={{ color: 'white', fontSize: vh(2) }}>Select an image from your camera roll</Text>
        </TouchableOpacity>
    {progress &&<Progress.Bar indeterminate={true} indeterminateAnimationDuration={1000} width={vw(90)} height={vh(2.5)} color={'orange'} animated={true}/>}
        {bodyText != "" ? image &&
          <View style={{ flexDirection: 'row-reverse' }}>
          {progress ||  
          <TouchableOpacity style={styles.submit} onPress={submit}>
              <Text style={{ color: 'white', fontSize: 20 }}>Submit</Text>
            </TouchableOpacity>}
            <TouchableOpacity style={styles.cancel} onPress={restart}>
              <Text style={{ color: 'white', fontSize: 20 }}>Cancel</Text>
            </TouchableOpacity>
          </View> : <View></View>}
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  input: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "black",
    width: vw(90),
    height: vh(15),
    borderRadius: 5,
    marginTop: vh(0.5)
  },
  buttonContainer: {
    marginTop: vh(1),
    height: vh(8),
    flexDirection: 'column',
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: vw(90),
    borderRadius: 5,
    backgroundColor: "#5d5d6d"
  },
  submit: {
    marginTop: vh(1),
    margin: 2,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: vw(40),
    borderRadius: 5,
    backgroundColor: "orange"
  },
  cancel: {
    marginTop: vh(1),
    margin: 2,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: vw(40),
    borderRadius: 5,
    backgroundColor: 'gray'
  }
})
postScreen.navigationOptions = {
  title: 'Add new Post'
}