import React,{useEffect, useState} from 'react'
import {Text , View , TouchableOpacity , StyleSheet , AsyncStorage } from 'react-native'
import { vw, vh} from 'react-native-expo-viewport-units';
import axios from 'axios'

export default props => {
  const [email , setEmail] = useState()
  const [name , setName] = useState()
  const [absentNumber , setAbsentNumber] = useState()
  const [listAbsents , setListAbsents] = useState([])
  const [didMount , setDidMount] = useState(false)
  useEffect(()=>{
    if (!didMount){

    (async()=> {
     const asyncEmail =  await AsyncStorage.getItem('email')
     setEmail(asyncEmail)
     const asyncName = await AsyncStorage.getItem('name')
     setName(asyncName)
     const url = 'https://orange-coding.herokuapp.com/att/student_query'
     axios.request({
      url,
      method : 'GET',
      params: {
        name : asyncName
      }
     })
     .then(({data})=>{
       setAbsentNumber(data.absent_num)
       const list = Object.keys(data)
       setListAbsents(list)
     }).catch(err=>{
      //  alert(err)
     })
    })()
    setDidMount(true)   
    }
  })
  const signOut=()=>{
    (async()=>{
      await AsyncStorage.removeItem('email')
      await AsyncStorage.removeItem('admin_user')
      await AsyncStorage.removeItem('name') 
      setDidMount(true)
      props.navigation.navigate('WelcomeScreen')
    })()
  
  }
    return (
      <>
      <View style={{flex:1 , backgroundColor:'black' , alignItems:'center', justifyContent:'center'}}>
        <Text style={{ color:'white', fontSize : vh(4),marginBottom:10}}>{name}</Text>
        <Text style={{ color:'white', fontSize : vh(2), marginBottom:10}}>{email}</Text>
        <Text style={{ color:'white', fontSize : vh(2) , marginBottom:10}}>{absentNumber != null ? absentNumber == 0 ? 'No any absents' : absentNumber == 1 ?`You have only one absence` : `You have ${absentNumber} Absences` : ''} </Text>

        {listAbsents.map((item,i)=>{
          if(item != 'absent_num')
          return <Text key={i} style={{color:'white',fontSize : vh(2)}}>{item}</Text>
        })}

        
        <TouchableOpacity onPress={signOut} style={styles.buttonContainer}>
       <Text  style={{color:'white', fontSize:20}}>sign out</Text>
        </TouchableOpacity>
        </View>

      </>
    )
  
}

const styles = StyleSheet.create({
  input: {
      // lineHeight: 20,
      borderStyle: "solid",
      borderWidth: 1,
      borderColor: "black",
      width: vw(90),
      height: vh(6),
      borderRadius: 5,
      marginTop: vh(0.5)
  },
  buttonContainer: {
      marginTop: vh(40),
      height: 45,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 20,
      width: "90%",
      borderRadius: 5,
      backgroundColor: "#f19a12"
    }
})
