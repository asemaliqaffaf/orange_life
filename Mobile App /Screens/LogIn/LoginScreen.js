import React, { useState } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet , ScrollView , AsyncStorage} from 'react-native'
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import Carousel from 'react-native-smart-carousel';
import axios from 'axios'

export default LoginScreen = (props) => {
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const datacarousel =[
        {
          "id": 339964,
          "title": "",
          "imagePath": "https://scontent.famm3-1.fna.fbcdn.net/v/t1.0-9/72328055_134818197905856_9010927634246270976_n.jpg?_nc_cat=105&_nc_eui2=AeFIdmPGHm9ij0WUOkUP5rghQS05_g8JeG0tBuQPGFNzCtJplaQucLu-TWRINRBV5SCIBCQjBisr0mm5VpJtmPJ94GSmjNVD9CI7OZ4czGjDiQ&_nc_ohc=51xx0E0Fe-kAQnPC3ePofst3vgSP-4oGF4ZnrwRtrswJtrlJteFYYNpeA&_nc_ht=scontent.famm3-1.fna&oh=c6f15ed5e65a07d5f493c8eb6caea17a&oe=5EB1A9E1", // URL
        },
        {
          "id": 3399644,
          "title": "",
          "imagePath": "https://scontent.famm3-2.fna.fbcdn.net/v/t1.0-9/70161082_115129756541367_705915537665818624_n.jpg?_nc_cat=101&_nc_eui2=AeGnxZ3cC5wFwaEzpkdjV-qZSYqaBuT__9TVneZQd_yRSXYUslYm0K7wGaaab4R-YTyaLkWdkMVjqNdnra1yHFtAIbdhc6kZ0mcWLw8Ow2pbmg&_nc_ohc=xi48pwTJYZ4AQmMSVwj7iuovXK8Ulm5uAJeU4AaUUjFny0AF4BAr8OBjA&_nc_ht=scontent.famm3-2.fna&oh=705c9b1fb885b47080a4daca33143ad3&oe=5EA32EB3", // URL
        },
        {
          "id": 339403,
          "title": "Orange Coding Life ",
          "subtitle": "This app made for Orange Coding Academy",
          // "imagePath": Image
        }
      ]
    const loginHandler = (text, name) => {
        const regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/
        const regexPassword = /^[0-9a-zA-Z]{8,}$/
        const event = text.toLowerCase().trim()
        if(name == 'email'){
            const testEmail = undefined
            if (regexEmail.test(event)){
                setEmail(event)
            }else{
                setEmail(testEmail)
            }
        }
        if(name == 'password'){
            const testPassword = undefined
            if(regexPassword.test(event)){
                setPassword(event)
            }else{
                setPassword(testPassword)
            }
        }
    }
     const submit = ()=>{
        if(email != null && password != null){

            const url = 'https://orange-coding.herokuapp.com/usr/auth'
            axios.request({
                url,
                method: 'POST',
                data :{
                    email,
                    password
                }
            }).then(async({data})=>{
                if(data.name !=null){
                await AsyncStorage.setItem('name' , data.name )    
                }
                if(data.email != null){
                await AsyncStorage.setItem('email' , data.email )
                }
                if(data.admin_user != null){
                await AsyncStorage.setItem('admin_user' , data.admin_user)
                }
                if(data.admin_user == null){
                await AsyncStorage.removeItem('admin_user')
                }
                props.navigation.navigate('tabNav')
            }).catch(err=>{
               alert(err)
            })
        
        }
     
    }
    return (
        <>
        <ScrollView >
        
        <View style={{ backgroundColor:'white' ,flex: 1 , justifyContent:'flex-start'}}>
        <Carousel
          data={datacarousel}
          onPress={(event) => { console.log(event) }}
          autoPlay={true}
          titleColor='#ff5c00'
          playTime= {2000}
          // parallax={true}
          height = {vh(30)}
          width={vw(100)}
        />
      </View>
    
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <TextInput style={styles.input} placeholder='Email Address' placeholderTextColor='gray' onChangeText={(text) => loginHandler(text, 'email')} />
            <TextInput style={styles.input} placeholder='Password' placeholderTextColor='gray' secureTextEntry={true} onChangeText={(text) => loginHandler(text, 'password')} />
            <TouchableOpacity style={styles.buttonContainer} onPress={submit}>
            <Text style={{color:'white' , fontSize:25}}>Log in</Text>
            </TouchableOpacity>
        </View>

        </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    input: {
        // lineHeight: 20,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "black",
        width: vw(90),
        height: vh(6),
        borderRadius: 5,
        marginTop: vh(0.5)
    },
    buttonContainer: {
        marginTop: vh(1),
        height: 45,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
        width: "90%",
        borderRadius: 5,
        backgroundColor: "#f99a12"
      }
})
LoginScreen.navigationOptions = {
    title: "Orange Coding Life"
};
