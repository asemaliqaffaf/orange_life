/*  _                          ___         __  __        __ 
   / \   ___  ___ _ __ ___    / _ \  __ _ / _|/ _| __ _ / _|
  / _ \ / __|/ _ \ '_ ` _ \  | | | |/ _` | |_| |_ / _` | |_ 
 / ___ \\__ \  __/ | | | | | | |_| | (_| |  _|  _| (_| |  _|
/_/   \_\___/\___|_| |_| |_|  \__\_\\__,_|_| |_|  \__,_|_|  
*/
import React from "react";
import "./Style/App.css";
import LoginScreen from "./components/LoginScreen"
import {BrowserRouter, Route, Switch, Router} from "react-router-dom"
import Notfound from './components/notfound'
import Calender from "./components/Calender"
import Search from "./components/Search"
import Register from "./components/Register" 
import SideMenu from "./components/SideMenu"
import Home from "./components/Home"
import DailyCheck from "./components/DailyCheck/DailyCheck"
import StudentQuery from "./components/DailyCheck/StudentQuery"
import StudentReportList from './components/fullReport/StudentReportList'
import FullStudentReport from './components/fullReport/FullStudentReport'
import ChangePassword from './components/ChangePassword'
import DeleteUser from "./components/DeleteUser"
import ScanQR from './components/ScanQR'
import Footer from "./components/footer/Footer";
export default ()=>{  
    return(
    <React.Fragment >
        <BrowserRouter>
        <div style={{ webkitTransitionDuration: '0.4s' , transitionDuration: '0.4s',position:'relative', float:'left' }}>
        <SideMenu></SideMenu>
        </div>
        <div style={{webkitTransitionDuration: '0.4s' , transitionDuration: '0.4s',position:'relative', display : 'flex', flexDirection:'column' , justifyContent:'center' , alignItems:'center' , margin: '0 auto' }}>
        <div style={{webkitTransitionDuration: '0.4s' , transitionDuration: '0.4s'}} className="App">      
        <Switch>
        <Route exact={true} path="/" component={LoginScreen} />
        <Route path="/daily" component={DailyCheck}/>
        <Route path="/query" component={StudentQuery}/>
        <Route path="/home" component={Home}/>
        <Route path="/Search" component={Search} />
        <Route path="/calender" component={Calender}/>
        <Route path="/register" component={Register}/>
        <Route path='/StudentReportList' component={StudentReportList}/>
        <Route path='/FullStudentReport' component={FullStudentReport}/>
        <Route path='/ChangePassword' component={ChangePassword}/>
        <Route path="/DeleteUser" component={DeleteUser}/>
        <Route path="/ScanQR" component={ScanQR}/>
        <Route component={Notfound} />   
        </Switch>
        </div>
        </div>
        </BrowserRouter>
        <Footer/>
      </React.Fragment>
    );
}