import React, { Component } from 'react'
import "./../Style/App.css"
import { Redirect } from "react-router-dom"
import cookie from "react-cookies"
import "./../Style/App.css"
import axios from 'axios'
import Home from './Home'
import Person from './../img/perosn.png'
class LoginScreen extends Component {
    state = {
        email: null,
        password: null,
        isAuth: cookie.load('isAuth'),
        signInClicked: false,
        validationEmail: false,
        validationPassword: false
    }
    handleHandler = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    }
    

    signIn = (e) => {
        const regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/
        const regexPassword = /^[0-9a-zA-Z]{8,}$/
        this.setState({
            signInClicked: true,
            validationEmail: false,
            validationPassword: false
        })
        if (regexEmail.test(this.state.email)) {
            this.setState({
                validationEmail: true
            })
        }
        if (regexPassword.test(this.state.password)) {
            this.setState({
                validationPassword: true
            })
        }
        if (e.key == 'Enter') {
            if (regexEmail.test(this.state.email) && regexPassword.test(this.state.password)) {
                const url = 'https://orange-coding.herokuapp.com/usr/auth'
                axios.request({ url, method: 'POST', data: { email: this.state.email, password: this.state.password } })
                    .then(res => { 
                        cookie.save('isAuth', true)
                        if (res.data.admin_user != null) {
                            cookie.save('user_Privileges', res.data.admin_user)
                            localStorage.setItem('viewQR' , true)
                        }
                        if(res.data.name != null){
                            localStorage.setItem('name' , res.data.name)
                        }
                        if(res.data.admin_user == null){
                            localStorage.removeItem('viewQR')
                        }
                        this.setState({
                            isAuth: true
                        })
                        localStorage.setItem('selectedMenu', 0)
                        return <Redirect to='/home' />
                    })
                    .catch(error => {
                        error.response.data.Error ? alert(error.response.data.Error) : alert(error)
                        console.log(error)
                        this.setState({
                            isAuth: false
                        })
                    })
            }
        }
    }
    render() {
        if (cookie.load('isAuth')) {
            return <Home />
        }
        else
            return (
                <React.Fragment>
                <img style={{ width: 10 + 'vw', marginTop: '5vh' }} src={Person}></img>
                    <div style={{ display: "flex", flexDirection: "column", flexWrap: "wrap", backgroundColor: "", alignItems: "center" , marginTop:'5vh'}}>
                        <div style={{ color: 'red' }}>
                            <input placeholder="Email Address" className={"login__input"} name='email' onChange={this.handleHandler}></input><br></br>
                            {this.state.signInClicked ? this.state.validationEmail ? '' : '* Required field' : ''}
                        </div>
                        <div style={{ color: 'red' }}>
                            <input placeholder="Password" className={"login__input"} name='password' onChange={this.handleHandler} onKeyPress={this.signIn} type="password"></input><br></br>
                            {this.state.signInClicked ? this.state.validationPassword ? '' : '* Required field' : ''}
                        </div>
                        <button style={{ padding: '10px', marginTop: '15px' }} className={"button button1"} type="submit" onClick={() => this.signIn({ key: 'Enter' })}> Log-In</button>
                    </div>
                </React.Fragment>
            );
    }
}

export default LoginScreen;