import React, { Component } from 'react'
import './../Style/SideMenu.css'
import { Link, Redirect } from "react-router-dom";
import cookie from "react-cookies"
import SearchIcon from "./../img/search.png"
import HomeIcon from "./../img/home.png"
import CalenderIcon from "./../img/calender.png"
import AddUserIcon from "./../img/add.png"
import LogOutIcon from "./../img/logout.png"
import DailyIcon from "./../img/daily.png"
import AuthIcon from './../img/auth.png'

export default class componentName extends Component {
  state = {
    hideMenu: false,
    selectedMenu: localStorage.getItem('selectedMenu')
  }
  signOut = () => {
    cookie.remove('isAuth', false)
    cookie.remove('user_Privileges')
    localStorage.removeItem('StudentName')
    localStorage.removeItem('name')
    localStorage.removeItem('viewQR')
    localStorage.removeItem('selectedMenu')
    this.setState({selectedMenu : null})
    return <Redirect to='/' />
  }
  hideMenuHandler = () => {
    this.setState({
      hideMenu:  !this.state.hideMenu
    })
  }
  num = 10
  render() {
    return (
      <>
<a onClick={this.hideMenuHandler }>
          <img style={{ webkitTransitionDuration: '0.5s', transitionDuration: '0.5s', left: this.state.hideMenu ? '.9vw' : '13vw' }} className="hide-button" src="https://cdn4.iconfinder.com/data/icons/gray-toolbar-7/512/hide_menu-512.png"></img>
        </a>
        <div style={{ webkitTransitionDuration: '0.4s', transitionDuration: '0.4s', display: this.state.hideMenu ? 'none' : '' }} className="sidenav-two">
      </div>
        <div style={{ webkitTransitionDuration: '0.4s', transitionDuration: '0.4s', display: this.state.hideMenu ? 'none' : '' }} className="sidenav">
          <Link to="/"onClick={() => {this.setState({selectedMenu:0}) 
          localStorage.setItem('selectedMenu', 0) }}>
            <center >
              <img className='logo' src={"https://upload.wikimedia.org/wikipedia/commons/c/c8/Orange_logo.svg"}></img>
            </center>
          </Link>
          <Link to="/home" className={"button-side-menu"} onClick={() => {this.setState({selectedMenu:0}) 
          localStorage.setItem('selectedMenu', 0) }}>
            <img className='thumbnail' src={HomeIcon}></img>
            <span style={this.state.selectedMenu == 0 ? { fontWeight: 'bold', color: 'white', marginLeft: '20px' } : {}} className="menu-link">
              Home
          </span> </Link>
          <Link to="/daily" className={"button-side-menu"} onClick={() => { 
            this.setState({selectedMenu:1})
            localStorage.setItem('selectedMenu', 1) }}>
            <img className='thumbnail' src={DailyIcon}></img>
            <span style={this.state.selectedMenu == 1 ? { fontWeight: 'bold', color: 'white', marginLeft: '20px'  } : {}} className="menu-link">
            Attendance
        </span> </Link>
          <Link to="/Search" className={"button-side-menu"} onClick={() => { 
                this.setState({selectedMenu:2})
            localStorage.setItem('selectedMenu', 2) }}>
            <img className='thumbnail' src={SearchIcon}></img>
            <span style={this.state.selectedMenu == 2 ? { fontWeight: 'bold', color: 'white', marginLeft: '20px' } : {}} className="menu-link">
              Search
      </span> </Link>
          <Link to="/calender" className={"button-side-menu"} onClick={() => { 
                this.setState({selectedMenu:3})
            localStorage.setItem('selectedMenu', 3) }}>
            <img className='thumbnail' src={CalenderIcon}></img>
            <span style={this.state.selectedMenu == 3 ? { fontWeight: 'bold', color: 'white', marginLeft: '20px' } : {}} className="menu-link">
              History
          </span> </Link>
          <Link to="/register" className={"button-side-menu"} onClick={() => { 
                this.setState({selectedMenu:4})
            localStorage.setItem('selectedMenu', 4) }}>
            <img className='thumbnail' src={AddUserIcon}></img>
            <span style={this.state.selectedMenu == 4 ? { fontWeight: 'bold', color: 'white', marginLeft: '20px' } : {}} className="menu-link">
              Register
          </span> </Link>
          <Link to="/ScanQR" className={"button-side-menu"} onClick={() => { 
                this.setState({selectedMenu:5})
            localStorage.setItem('selectedMenu', 5) }}>
          <img className='thumbnail' src={AuthIcon}></img>
          <span style={this.state.selectedMenu == 5 ? { fontWeight: 'bold', color: 'white', marginLeft: '20px' } : {}} className="menu-link">
            Scan QR
        </span> </Link>
          <Link to="/" style={{ marginTop: "30vh" }} className={"button-side-menu"} onClick={this.signOut} >
            <img className='thumbnail' src={LogOutIcon}></img>
            <span className="menu-link">
              {!cookie.load('isAuth') ? 'Sign In' : 'Sign Out'}
            </span></Link>
           
        </div>
      </>
    )
  }
}