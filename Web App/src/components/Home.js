import React from 'react'
import { Link } from 'react-router-dom'
import cookie from "react-cookies"
import moment from "moment"
import Events from "./../img/Events.png"
import AddAccount from "./../img/addAccount.png"
import Report from "./../img/report.png"
import HistoryIcon from "./../img/history.png"
import Search from "./../img/searchHome.png"
import QR from "./../img/QR.png"
import ChangePass from "./../img/changePass.png"
import DeleteAcc from "./../img/deleteAcc.png"
import Person from './../img/perosn.png'
import './../Style/Home.css'
import 'font-awesome/css/font-awesome.min.css';

export default function Home(){
    const name = localStorage.getItem('name')
    const duration = moment().format('HH') >=1 && moment().format('HH') <=11 ? 'Morning' :'Evening' ;
    if (!cookie.load('isAuth')) {
      return (
        <div style={{marginTop:'10vh'}}>
          <h2>You don't have permission to access Home page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
        </div>
      )
    } else{
      return (
        <>


          <img style={{ width: 10 + 'vw', marginTop: '1vh' }} src={Person}></img>
          <p style={{ color:'white' ,fontWeight:'lighter' ,fontSize: 45 }}>Good {`${duration} ${name}`}</p>
          <div className={'homeContainer'} >
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/daily">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={Events}></img>
              </Link>
             <div style={{ flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}> Daily Attendance</div>

        </span>
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/register">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={AddAccount}></img>
              </Link>
             <div style={{flexWrap:'wrap', flexDirection:'column' , fontSize: '20px'}}>  Add new user</div>
        </span>
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/StudentReportList">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={Report}></img>
              </Link>
             <div style={{flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}> Full Report</div>
          </span>
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/calender">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={HistoryIcon}></img>
              </Link>
             <div style={{flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}>  History</div>
    </span>
          </div>
          <div className={'homeContainerTwo'} >
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/Search">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={Search}></img>
              </Link>
             <div style={{flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}>  Search</div>
      </span>
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/ScanQR">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={QR}></img>
              </Link>
             <div style={{flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}>   Scan QR</div>
      </span>
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/ChangePassword">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={ChangePass}></img>
              </Link>
             <div style={{flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}>   Change Password</div>
  </span>
            <span style={{ display: "flex", flexDirection: "column" }}>
              <Link to="/DeleteUser">
                <img style={{ width: 10 + 'vw', borderRadius: 30 + 'px', margin: 20 + 'px' }} src={DeleteAcc}></img>
              </Link>
              <div style={{flexWrap:'wrap', flexDirection:'column', fontSize: '20px'}}>   Delete User</div>
  </span>
          </div>

        </>
      )}
}
