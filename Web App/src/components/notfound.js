import React from 'react'
import {Link} from "react-router-dom"
import "./../Style/App.css"
const Notfound = () =><div> <img style={{width:"300px"}} src={"https://images.vexels.com/media/users/3/134775/isolated/preview/fe9e24622975f54832c4345e683e5f93-sick-emoji-emoticon-by-vexels.png"}/> 
<div className={"not-found"}>
<span>Sorry<br></br>
The address that you’re looking for doesn’t exist anymore.
<br></br><Link style={{color:"lightblue"}} to="/">home page</Link>
</span>
</div></div>
export default Notfound