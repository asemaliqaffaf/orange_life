import Autosuggest from 'react-autosuggest';
import React from "react"
import "./../Style/App.css"
import cookie from "react-cookies"
import { Link } from "react-router-dom"
import axios from 'axios'
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { DateRangePicker } from 'rsuite';
import 'rsuite/dist/styles/rsuite-dark.css'
import moment from "moment"

// import 'rsuite/dist/styles/rsuite-dark'
// import 'rsuite/lib/styles/index.less'; // or 'rsuite/dist/styles/rsuite-default.css'

export default  class Search extends React.Component {
  state = {
    value: '',
    suggestions: [],
    names: {},
    list: [],
    data: [],
    durationShown : false,
    fromDate : null,
    toDate : null
  };
  onChange = (event, { newValue }) => {
    if (newValue == "") {
      this.setState({ data: [] })
    }
    this.setState({
      value: newValue,
    });

  }
  componentDidMount() {
    if (cookie.load('isAuth')) {
    this.fetchStudentsList()
    }
  }
  fetchStudentsList = async() => {
    const userPrivileges = await cookie.load('user_Privileges')
    const url = 'https://orange-coding.herokuapp.com/att/students_list'
    axios.request({
      url,
      method: 'GET',
      auth: {
        username: "admin",
        password: userPrivileges
      }
    })
      .then(({ data }) => {
        this.setState({
          list: data
        })

      })
      .catch(error => {
        error.response.data.Error ? alert(error.response.data.Error) : alert(error)
      })
  }
  pressEnterHandler = (event) => {
    if (event.key == 'Enter') {
      // console.log(this.state.value)
      if (this.state.value != "") {
        this.fetchSelectedName(this.state.value , [])
      }
    }

  }
  // ===================== phase two
  getDurationDates = (startDate, stopDate) => {
    var dateArray = [];
    var currentDate = new moment(startDate,'DD-MM-YYYY');
    var stopDate = new moment(stopDate,'DD-MM-YYYY');
    while (currentDate <= stopDate) {
        dateArray.push( moment(currentDate).format('dddd:DD-MM-YYYY') )
        currentDate = moment(currentDate).add(1, 'days');
    }
    
    this.setState({toDate : moment(stopDate).format('dddd:DD-MM-YYYY'),
    fromDate :  moment(startDate).format('dddd:DD-MM-YYYY')})
    this.fetchSelectedName(this.state.value , dateArray)
}
// ======================== phase two
deleteAttendanceDate = async (date)=>{
// console.log(Object.keys(date)[0])
const url = 'https://orange-coding.herokuapp.com/att/delete_attendance_date'
    let data = { name : this.state.value , date : Object.keys(date)[0]  }
    // console.log(data)
    const userPrivileges = await cookie.load('user_Privileges')

    if (this.state.value == "") {
      this.setState({ data: [] })
    } else {
      axios.request({
        url,
        method: 'DELETE',
        data,
        auth: {
          username: "admin",
          password: userPrivileges
        }
      })
        .then(({ data }) => {
          // console.log(data)
          this.getDurationDates(this.state.fromDate,this.state.toDate)
        })
        .catch(err => {
          // this.setState({ data: [] })
          console.log(err)
        })
    }
}
  fetchSelectedName = async (name , dateArray) => {
    this.setState({durationShown : true})


    const userPrivileges = await cookie.load('user_Privileges')
    
    const url = 'https://orange-coding.herokuapp.com/att/duration_query'
    let data = { name , duration_array : dateArray }
    // console.log(data)
    if (this.state.value == "") {
      this.setState({ data: [] })
    } else {
    await  axios.request({
        url,
        method: 'POST',
        data,
        auth: {
          username: "admin",
          password: userPrivileges
        }
      })
        .then(({ data }) => {
          this.setState({ data })
        })
        .catch(err => {
          this.setState({ data: [] })
          console.log(err)
        })
    }
  }

  getSuggestionValue = suggestion => suggestion;

  renderSuggestion = suggestion => (
    <div onClick={() => this.fetchSelectedName(suggestion , [])} >
      {suggestion}
    </div>
  )
  getSuggestions = value => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;
    const uniqueNames = this.state.list
    return inputLength == 0 ? [] : uniqueNames.filter(lang =>
      lang.slice(0, inputLength) == inputValue
    );
  };


  onSuggestionsFetchRequested = ({ value }) => {
    // this.setState({durationShown : false})

    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };


  render() {

    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Type a name of trainee',
      value,
      onChange: this.onChange,
      onKeyDown: this.pressEnterHandler
    };
    if (!cookie.load('isAuth')) {
      return (
        <div style={{marginTop:'10vh'}}>
          <h2>You don't have permission to access Search page<br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
        </div>
      )
    } else
      return (
        <React.Fragment>
          <h1>Search Page</h1>
         
          <div style={{color:'black'}}>
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps} />
            </div>
          <div>
          {this.state.durationShown  &&
          <DateRangePicker 
          style={{ width: 380,marginTop : '20px' , marginBottom : '20px' }}
          onChange={value => {
            this.getDurationDates(moment(value[0]).format('DD-MM-YYYY') ,moment(value[1]).format('DD-MM-YYYY') )
          }}
          /> }
      
          {this.state.data.length > 0 && 
              <div>
              <div style={{color:'black'}}>
              <ReactHTMLTableToExcel
              id="test-table-xls-button"
              className="download-table-xls-button"
              table="table-to-xls"
              filename={`report for: ${this.state.value}`}
              sheet="tablexls"
              buttonText="Download as Excel file"/>        
              </div>     
              <table id="table-to-xls" style={{ marginTop: '5vh' }}>
              <h2 style={{display:'none'}}>{`Full Report for: ${this.state.value}`}</h2>
              <h2 style={{display:'none'}}>{`Duration from : ${this.state.fromDate} to ${this.state.toDate}`}</h2>

                <tr>
                  <th>Date</th>
                  <th>Check in</th>
                  <th>Check out</th>
                  <th>Edit</th>
                </tr>
                {this.state.data.map((item,i)=>{
                    
                    return ( <tbody key={i}>
                      {typeof item[Object.keys(item)[0]] == 'string' ? <tr style={{color:'red'}}><td>{[Object.keys(item)[0]]}</td><td>absent</td><td>absent</td><td>---</td></tr>:
                      <tr> <td>{[Object.keys(item)[0]]}</td> <td>{item[Object.keys(item)[0]].in}</td>  <td >{item[Object.keys(item)[0]].out}</td>        
                      <td><button style={{color:'black',background:'crimson'}} onClick={()=>this.deleteAttendanceDate(item)}>X</button></td>
                      </tr>
                    }
                      </tbody>)
                  // }
                })}
               
              

              </table>  </div>}
          </div>
        </React.Fragment>
      );
  }
}