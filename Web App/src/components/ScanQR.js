import React, { Component } from 'react'
import QRCode  from "qrcode.react"; 
import moment from 'moment-timezone'
import cookie from "react-cookies"
import axios from 'axios'

export default class ScanQR extends Component {
  state={
    hash : "",
    intervalId : null,
    currentCount: null
  }
  componentDidMount(){
    var intervalId = setInterval(this.timer, 5000);
    this.setState({intervalId: intervalId});
    this.syncQRValue()
  }

  fetchDailyCheckData = async () => {
    const userPrivileges = await cookie.load('user_Privileges')
 
    let url = 'https://orange-coding.herokuapp.com/att/students_daily_query'
    axios({
      method: 'get',
      url,
      auth: {
        username: "admin",
        password: userPrivileges
      }
    })
      .then(({data}) => {
 
        // console.log(data)
      })
      .catch((err) => {
        err.response.data.Error ? alert(err.response.data.Error) : alert(err)
        console.log("error!!!! ", err)
      })

  }

  syncQRValue=()=>{

    const LA =moment().tz("America/Los_Angeles").format('MM70HH mmDD98');
    let u = Date.now().toString(16) + Math.random().toString(16) + '0'.repeat(16);
    let uuidNumbers = [u.substr(13,3), u.substr(16,12)].join('-');

    this.setState({
      hash : `${uuidNumbers}_${LA}`
    })
  }

timer= ()=>{
  this.setState({ currentCount: this.state.currentCount -1 });
  this.syncQRValue()
  this.fetchDailyCheckData()
}
signOut = () => {
  cookie.remove('isAuth', false)
  cookie.remove('user_Privileges')
  localStorage.removeItem('StudentName')
  localStorage.removeItem('name') 
  window.location.reload();
}

  render() {
  if(localStorage.getItem('viewQR') == null){
        return (
          <div style={{ marginTop: '10vh' }}>
              <h2>You don't have permission to access this page <br></br>
           <h2>You are not Authorized to access this page!</h2>
              </h2>
          </div>
      )
  }
    return (
      <>
     <h1>Scan QR in order to check in/out</h1>
     {cookie.load('user_Privileges') && <button onClick={()=>this.signOut()} className={'button button3'}>{`Leave & Keep QR on`}</button>}
     <QRCode className="qr" value={this.state.hash}/><br/> 
      </>
    )
  }
}
