import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './../DailyCheck/tabel.css'
import StudentList from './StudentsList'
import cookie from "react-cookies"
import axios from 'axios'
export default class StudentReport extends Component {
  state ={
    data : []
  }
  componentDidMount=()=>{
    if (cookie.load('isAuth')) {
    this.fetchStudentsList()
    }
  }
  fetchStudentsList=async()=>{
    const userPrivileges = await cookie.load('user_Privileges')
    const url = 'https://orange-coding.herokuapp.com/att/students_list'
    axios.request({
      url,
      method:'GET',
      auth: {
        username: "admin",
        password: userPrivileges
      }
    })
    .then(({data})=>{
      this.setState({
        data 
      })
      // console.log(res.data)
    })
    .catch(err=>{
      console.log(err)
      err.response.data.Error ? alert(err.response.data.Error) : alert(err)

    })
  }
  render() {
    if (!cookie.load('isAuth')) {
      return (
        <div style={{marginTop:'10vh'}}>
          <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
        </div>
      )
    } else
    return (
      <>
      <h1>Student Report</h1>
      <div className={'daily-table'}>
        <table>
        <tr>
        <th>Number</th>
          <th> Name <span style={{ fontSize: '15px', color: 'tomato' }}> click on any name to see their history</span>
          </th>
        </tr>
        {this.state.data.map((student,index)=>{
          if(student != '_id' && student != 'trainee')
          return   <StudentList  student={student} index={index}> </StudentList> 
       })}

      

      </table>
</div>
      </>
    )
  }
}
// return  <tr onClick={()=>this.onClickHandler(student)}> <td><Link className='link' to='/FullStudentReport'> {index-1} </Link></td><Link className='link' to='/FullStudentReport'> <td>{student}</td></Link></tr> 
