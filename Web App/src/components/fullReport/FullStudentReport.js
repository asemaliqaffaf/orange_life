import React, { Component } from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import cookie from "react-cookies"
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

export default class FullStudentReport extends Component {
    state={
        name : localStorage.getItem('StudentName'),
        data : []
    }
    componentDidMount(){
      if (cookie.load('isAuth')) {
    this.fetchFullStudentReport()
      }
    }
    fetchFullStudentReport=async()=>{
      const userPrivileges = await cookie.load('user_Privileges')
      let url = 'https://orange-coding.herokuapp.com/att/full_report'
      let data = {name : this.state.name}
      axios.request({
        url,
        method:'POST',
        data,
        auth: {
          username: "admin",
          password: userPrivileges
        }
      })
      .then(({data})=>{
        this.setState({data : data[0]})
      })
      .catch(err=>{
        console.log(err)
        err.response.data.Error ? alert(err.response.data.Error) : alert(err)


      })
    }
  render() {
    if (!cookie.load('isAuth')) {
      return (
        <div style={{marginTop:'10vh'}}>
          <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
        </div>
      )
    } else
    return (
      <>
     
        <h2>{`Full Report for: ${this.state.name}`}</h2>
        <div style={{color:'black'}}>
        <ReactHTMLTableToExcel
        id="test-table-xls-button"
        className="download-table-xls-button"
        table="table-to-xls"
        filename={`report for: ${localStorage.getItem('StudentName')}`}
        sheet="tablexls"
        buttonText="Download as Excel file"/>
</div>
        <div className={'daily-table'}>

        <table id="table-to-xls">
        <h2 style={{display:'none'}}>{`Full Report for: ${this.state.name}`}</h2>
        <tr>
          <th>Date</th>
          <th>Check in</th>
          <th>Check out</th>
          
        </tr>
      
        {Object.keys(this.state.data).map(date=>{
         if(date != '_id' && date != 'trainee')
          return (<tr> <td>{date}</td>{this.state.data[date] != 'absent' ? <td>{this.state.data[date].in}</td> : <td style={{color:'red'}}>absent</td> }
          {this.state.data[date] != 'absent' ? <td>{this.state.data[date].out}</td> : <td style={{color:'red'}}>absent</td> }
          </tr>
           )
        })}
       
   
      </table>
      
      </div>
      </>
    )
  }
}
