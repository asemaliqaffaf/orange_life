import React from 'react'
import {Link} from 'react-router-dom'
import cookie from "react-cookies"

export default function StudentList(props) {
    const onClickHandler=(event)=>{
        localStorage.setItem('StudentName' , event)
      }
      if (!cookie.load('isAuth')) {
        return (
          <div style={{marginTop:'10vh'}}>
            <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
          </div>
        )
      } else
    return (
      
      <>
      <tr onClick={()=>onClickHandler(props.student)}> 
        <td><Link className='link' to='/FullStudentReport'> {props.index+1} </Link></td>
        <Link className='link' to='/FullStudentReport'> <td>{props.student}</td></Link>
        </tr> 
      </>
    )
  }
