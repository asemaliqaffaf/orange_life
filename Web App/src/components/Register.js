import React, { Component } from 'react'
import "./../Style/App.css"
import { Link } from "react-router-dom"
import './DailyCheck/ButtonStyle.css'
import axios from 'axios'
import cookie from "react-cookies"
import Person from './../img/perosn.png'

class Register extends Component {
    state = {
        name: null,
        email: null,
        nationalId:null,
        password: null,
        confirmPassword: null,
        disagree: true,
        registerClicked: false,
        validationName: false,
        validationEmail: false,
        validationPassword: false,
        validationConfirmPassword: false,
        checkedAdmin: false,
        adminPrivileges: false
    }
    check = (event) => {
        if (event.target.value == "agree") {
            this.setState({
                disagree: false
            })
        }
        if (event.target.value == "disagree") {
            this.setState({
                disagree: true
            })
        }
    }
    
    handelOnChangeEmail = (event) => {
        let { name, value } = event.target
   
        this.state[name] = value
    }
    registerHandler = async () => {
        const userPrivileges = await cookie.load('user_Privileges')
        if (this.state.checkedAdmin == false) {
            this.setState({
                adminPrivileges: false
            })
        }

        const regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/
        const regexPassword = /^[0-9a-zA-Z]{8,}$/
        // const regexName = /^[A-Z,a-z]+[\s|,][A-Z,a-z]{1,19}$/
        let fullName = null
        let email = null
        let nationalId = this.state.nationalId
        let password = null
        let identical = false
       
        this.setState({
            registerClicked: true,
            validationName: false,
            validationEmail: false,
            validationPassword: false,
            validationConfirmPassword: false
        })
        if (this.state.name) {
            fullName = this.state.name
            this.setState({
                validationName: true
            })
        }
        if (regexEmail.test(this.state.email)) {
            email = this.state.email
            this.setState({
                validationEmail: true
            })
        }
        if (regexPassword.test(this.state.password)) {
            password = this.state.password
            this.setState({
                validationPassword: true
            })
        }
        if (this.state.password === this.state.confirmPassword && password != null) {
            identical = true
            this.setState({
                validationConfirmPassword: true
            })
        }

        if (identical && fullName != null && email != null) {
            await axios.request({
                url: 'https://orange-coding.herokuapp.com/att/add_new',
                method: 'post', data: { name: fullName },
                auth: {
                    username: "admin",
                    password: userPrivileges
                }
            }).then(async (res) => {
                const name = await res.data.trainee
                const userPrivileges = await cookie.load('user_Privileges')
                let data = { name, email,national_id: this.state.nationalId , password }
                if (this.state.adminPrivileges) {
                    data = { name, email, password, national_id : this.state.nationalId ,admin_user: this.state.adminPrivileges }
                }
         console.log({data})
                const url = 'https://orange-coding.herokuapp.com/usr/newuser'
                await axios.request({
                    url, method: 'post', data,
                    auth: {
                        username: "admin",
                        password: userPrivileges
                    }
                })
                    .then(res => {
                        // <==================== BASIC AUTH. =====================>
                        // if (res.data.admin_user != null) { 
                        //     cookie.save('user_Privileges', res.data.admin_user)
                        // }
                        console.log(res.data)
                    })
                    .catch(error => {
                        window.confirm(`This email address is already in use\n${error}`)
                    })

            })
                .catch(error => {
                    const test = window.confirm(`You already have this student name in your list\n${error}`)
                    console.log(test)
                })

                .then(() => {
                    // window.location.reload()
                })
        }
    }
    adminPrivilegesHandler = (event) => {
        if (this.state.checkedAdmin && event == 'Administrator') {
            this.setState({
                adminPrivileges: true
            })
        }
        else {
            this.setState({
                adminPrivileges: false
            })
        }
    }

    render() {
        if (cookie.load('user_Privileges') == null || !cookie.load('isAuth') ) {
            return (
                <div style={{ marginTop: '10vh' }}>
                    <h2>You don't have permission to access Register page <br></br>
                        {cookie.load('isAuth') ? <h2>You are not Authorized to access this page!</h2> : <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link>}
                    </h2>
                </div>
            )
        } else  {
            return (
                <React.Fragment>
                    <div>            <a>
                        <img style={{ width: 10 + 'vw', marginTop: 8 + 'vh' }} src={Person}></img>
                    </a>
                        <div style={{ display: "flex", flexDirection: "column", flexWrap: "wrap", backgroundColor: "", alignItems: "center" }}>
                            <input style={{color:'black'}} name='name' placeholder="Full Name" className={"login__input"} onChange={this.handelOnChangeEmail}></input>
                            <div style={{ color: 'red' }}> {this.state.registerClicked ? this.state.validationName ? '' : '* Required field' : ''}</div>
                            <input style={{color:'black'}} name='email' placeholder="Email" className={"login__input"} onChange={this.handelOnChangeEmail}></input>
                            <div style={{ color: 'red' }}> {this.state.registerClicked ? this.state.validationName ? '' : '* Required field' : ''}</div>
                            <input style={{color:'black'}} name='nationalId' placeholder="National id" className={"login__input"} onChange={this.handelOnChangeEmail}></input>
                            <div style={{ color: 'red' }}> {this.state.registerClicked ? this.state.validationEmail ? '' : '* Required field' : ''}</div>
                            <input style={{color:'black'}} type='password' name='password' placeholder="Password" className={"login__input"} onChange={this.handelOnChangeEmail}></input>
                            <div style={{ color: 'red' }}> {this.state.registerClicked ? this.state.validationPassword ? '' : '* Required field' : ''}</div>
                            <input style={{color:'black'}} type='password' name='confirmPassword' placeholder="Confirm Password" className={"login__input"} onChange={this.handelOnChangeEmail}></input>
                            <div style={{ color: 'red' }}> {this.state.registerClicked ? this.state.validationConfirmPassword ? '' : '* Required field' : ''}</div>
                            <label style={{ marginTop: "1%" }} > I agree to the <Link style={{ color: "lightblue" }} to="/terms"> terms and conditions</Link></label><br></br>
                            <div style={{ marginBottom: '3vh', fontSize: '15px', color: 'red' }}>
                                <input style={{color:'black'}} type="checkbox" checked={this.state.checkedAdmin} onChange={() => this.setState({ checkedAdmin: !this.state.checkedAdmin })} /> Make it Admin
                           {this.state.checkedAdmin ? <select onChange={(event) => this.adminPrivilegesHandler(event.target.value)}>
                                    <option selected value="Standard">Standard</option>
                                    <option value="Administrator" >Administrator</option>
                                </select> : <div></div>}
                            </div>
                            <select onChange={this.check}>
                                <option selected value="disagree">i don't accept the terms and con.</option>
                                <option value="agree" >i accept the terms and conditions</option>
                            </select>


                        </div>
                        <button onClick={this.registerHandler} disabled={this.state.disagree} style={{ marginTop: "1%", padding: this.state.disagree ? '0px' : '', color: this.state.disagree ? 'lightGray' : '' }} className={"button button1"}>Register</button>
                    </div>

                </React.Fragment>
            )
        }
    }
}

export default Register;