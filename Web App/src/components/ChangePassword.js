import React, { Component } from 'react'
import "./../Style/App.css"
import { Link } from "react-router-dom"
import cookie from "react-cookies"
import "./../Style/App.css"
import axios from 'axios'

class LoginScreen extends Component {
    state = {
        email: null,
        password: null,
        isAuth: cookie.load('isAuth'),
        signInClicked: false,
        validationEmail: false,
        validationPassword: false
    }
    handleHandler = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    }
    signIn = async(e) => {
        const userPrivileges = await cookie.load('user_Privileges')
        const regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/
        const regexPassword = /^[0-9a-zA-Z]{8,}$/
        this.setState({
            signInClicked: true,
            validationEmail: false,
            validationPassword: false
        })
        if (regexEmail.test(this.state.email)) {
            this.setState({
                validationEmail: true
            })
        }
        if (regexPassword.test(this.state.password)) {
            this.setState({
                validationPassword: true
            })
        }
        if (e.key == 'Enter') {
            if (regexEmail.test(this.state.email) && regexPassword.test(this.state.password)) {
                // const url = '/usr/auth'
                const url = 'https://orange-coding.herokuapp.com/usr/change_password'
                axios.request({ url, method: 'PUT', data: { email: this.state.email, password: this.state.password },
                auth: {
                    username: "admin",
                    password: userPrivileges
                } })
                    .then(res => {
                        alert(`The Password of ${res.data.email} has been changed`)
                        window.location.reload()
                    })
                    .catch(error => {
                        error.response.data.Error ? alert(error.response.data.Error) : alert(error)
                        console.log(error)
                    })
            }
        }
    }
    render() {
        if (cookie.load('user_Privileges') == null) {
            return (
                <div style={{ marginTop: '10vh' }}>
                    <h2>You don't have permission to access Register page <br></br>
                        {cookie.load('isAuth') ? <h2>You are not Authorized to access this page!</h2> : <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link>}
                    </h2>
                </div>
            )
        } else 
            return (
                <React.Fragment>
                    <div style={{ display: "flex", flexDirection: "column", flexWrap: "wrap", backgroundColor: "", alignItems: "center", marginTop: '15vh' }}>
                    <h1>Change Password</h1>    
                    <div style={{ color: 'red' }}>
                            <input placeholder="Email Address" className={"login__input"} name='email' onChange={this.handleHandler}></input><br></br>
                            {this.state.signInClicked ? this.state.validationEmail ? '' : '* Required field' : ''}
                        </div>
                        <div style={{ color: 'red' }}>
                            <input placeholder="Password" className={"login__input"} name='password' onChange={this.handleHandler} onKeyPress={this.signIn} type="password"></input><br></br>
                            {this.state.signInClicked ? this.state.validationPassword ? '' : '* Required field' : ''}
                        </div>
                        <button style={{ padding: '5px', marginTop: '15px' }} className={"button button1"} type="submit" onClick={() => this.signIn({ key: 'Enter' })}>Change Password</button>
                    </div>
                </React.Fragment>
            );
    }
}

export default LoginScreen;