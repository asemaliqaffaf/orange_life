import React, { Component } from 'react'
import axios from 'axios'
// import DailyList from "./DailyList"
import './tabel.css'
import "./ButtonStyle.css"
import { Link } from 'react-router-dom'
import cookie from "react-cookies"
import Popup from "reactjs-popup";
import moment from "moment"

export default class DailyCheck extends Component {
  state = {
    data: [],
  }
  checkInNowHandler = (student) => {

    // if (studentName.absent) {
        // let name = studentName.absent.trainee
        // console.log(student)
        const name = student.attend ? student.attend.trainee : student.absent.trainee;

        axios.request({
            method: 'patch',
            url: 'https://orange-coding.herokuapp.com/att/check_in',
            data: { name }
        })
            .then(({data}) => {
                this.setState({
                  data
                })
            })
            .catch(err => {
                err.response.data.Error ? alert(err.response.data.Error) : alert(err)
                console.log("error", err)
            })
    // }
}
checkOutNowHandler = (student) => {
  if (student.attend) {
      let name = student.attend.trainee
      // console.log(name)
      axios.request({
          method: 'put',
          url: 'https://orange-coding.herokuapp.com/att/check_out',
          data: { name }
      })
          .then(({data}) => { 
        //  console.log(data)
              this.setState({
                data  
              })
          })
          .catch(err => {
              err.response.data.Error ? alert(err.response.data.Error) : alert(err)
              console.log("error", err)
          })
    
          .then(()=>{
           
          })
  }
}
  componentDidMount() {
    if (cookie.load('isAuth')) {
      this.fetchDailyCheckData()
    }
  }
 
  fetchDailyCheckData = async () => {
    const userPrivileges = await cookie.load('user_Privileges')
 
    let url = 'https://orange-coding.herokuapp.com/att/students_daily_query'
    axios({
      method: 'get',
      url,
      auth: {
        username: "admin",
        password: userPrivileges
      }
    })
      .then(({data}) => {
        this.setState({
          data

        })
      })
      .catch((err) => {
        err.response.data.Error ? alert(err.response.data.Error) : alert(err)
        console.log("error!!!! ", err)
      })

  }
  
  studentSelectedHandler = (student) => {
    const name = student.attend ? student.attend.trainee : student.absent.trainee
    localStorage.setItem('StudentName', name)
}


  render() {
    if (!cookie.load('isAuth')) {
      return (
        <div style={{marginTop:'10vh'}}>
          <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
        </div>
      )
    } else{
      return (
        <>
          <h1>Daily Attendance</h1>
          <h3 >{ moment(Date()).format('dddd DD-MM-YYYY') }</h3>
          <div className={'daily-table'}>
            <table>
              <tr>
                <th> Name <span style={{ fontSize: '15px', color: 'tomato' }}> click on any name to see their history</span>
                
                </th>
                <th>Check-in</th>
                <th>Check-out</th>

              </tr>
              {this.state.data.map((student,i) => {
                return(
                  <tr key={i} onClick={() => this.studentSelectedHandler(student)} style={student.absent ? { color: 'red' } : {}}>

                  <td >
                      <Link style={student.absent ? { color: 'red' } : {}} className='link' to='/query'>
                          {student.attend ? student.attend.trainee : student.absent.trainee}
                      </Link></td>

                  <td>
                      {student.absent ?
                          <Popup trigger={<button style={{ padding: '2px' }} class="button button1">Check-in NOW</button>} position="top center">
                              {student.absent ? <div style={{ color: 'black', padding: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'center ', alignItems: 'center', alignContent: 'center' }}> <div>Are you sure you want to check in now?</div> <button  onClick={() => this.checkInNowHandler(student)} style={{ padding: '5px' }} class="button button1">Check in </button></div> : <div style={{ color: 'red', padding: '10px' }}>{`You've already check in`}</div>}
                          </Popup> :
                          <Link className='link' to='/query'>
                              {student.attend && student.attend[Object.keys(student.attend)[1]].in}
                          </Link>}
                  </td>
                  <td >
                      {student.attend &&
                              student.attend[Object.keys(student.attend)[1]].out ?    <Link className='link' to='/query'>{student.attend[Object.keys(student.attend)[1]].out }</Link>: <Popup trigger={<button style={{ padding: '2px' }} class="button button3">Check-out NOW</button>} position="top center">
                              <div style={{ color: 'black', padding: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'center ', alignItems: 'center', alignContent: 'center' }}> <div>Are you sure you want to check out now?</div> <button  onClick={() => this.checkOutNowHandler(student)} style={{ padding: '5px' }} class="button button3">Check out </button> </div>
                          </Popup>  }
                          
                  </td>
              
            
              </tr>


                )
              })}

            </table>
          </div>
        </>
      )
  }
  }
}

// {student.attend[Object.keys(student.attend)[1]].out}

// {this.state.data.map((student) => {
//   return <DailyList data={student} fetchDailyCheckData={this.fetchDailyCheckData}></DailyList>
// })}