import React, { Component } from 'react'
import TimeKeeper from 'react-timekeeper';
import './ButtonStyle.css'
import axios from 'axios';
import {Link} from 'react-router-dom'
import cookie from "react-cookies"
import moment from "moment"

export default class ModelEditAttendanceForStudent extends Component {
  state = {
    timeIn: null,
    timeOut: null
  }
  setTimeInHandler = (time) => {
    this.setState({
      timeIn: time.formatted24
    })
  }
  setTimeOutHandler = (time) => {
    this.setState({
      timeOut: time.formatted24
    })
  }
  editTimeSender = () => {

    if (this.state.timeIn != null &&  this.state.timeOut != null && this.props.selectedDate != null) {
      if (new Number(this.state.timeIn.split(':')[0]) < new Number(this.state.timeOut.split(':')[0])){
      let date = this.props.selectedDate
      let timeIn = this.state.timeIn
      let timeOut =  this.state.timeOut
      let name = localStorage.getItem('StudentName')
      axios.request({
        method: 'patch',
        url: 'https://orange-coding.herokuapp.com/att/check_in',
        data: {name, date, time:timeIn}
      })
        .then(res => {
          this.props.fetchStudentQuery()
        })
        .catch(err => {
          console.log(err)
          err.response.data.Error ? alert(err.response.data.Error) : alert(err)

        })
        .then(()=>{
          axios.request({
            method:'put',
            url : 'https://orange-coding.herokuapp.com/att/check_out',
            data : {name , date , time : timeOut}
          })
          .then(res=>{
            this.props.isVisible(!res.data)
            this.props.fetchStudentQuery()
          })
          .catch(err=>{
            console.log(err)
          })
        })
      }
    }

  }

  cancelHandler = () => {
    this.setState({
      timeIn: null,
      timeOut: null
    })


    this.props.isVisible(false)
  }
  render() {
    if (!cookie.load('isAuth')) {
      return (
        <div style={{marginTop:'10vh'}}>
          <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
        </div>
      )
    } else
    return (
      <>
        <div style={{ display: 'flex', flexDirection: 'row'  }}>
          <span>
            <p style={{fontSize:'23px'}}>
              Time in:
            </p>

            <TimeKeeper
              time={this.state.timeIn}
              onChange={(newTime) => this.setTimeInHandler(newTime)}
            />
          </span>
          <span>
          <p style={{fontSize:'23px'}}>
          Time out:
        </p>

            <TimeKeeper
              time={this.state.timeOut}
              onChange={(newTime) => this.setTimeOutHandler(newTime)}
            />
          </span>

        </div>
        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <button class="button button3" onClick={this.cancelHandler}>Cancel</button>
          <button className="button button1" onClick={this.editTimeSender} >Set</button>
        </div>
      </>
    )
  }
}
const style = {

}