import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import "./ButtonStyle.css"
import Popup from "reactjs-popup";
import axios from 'axios'
import cookie from "react-cookies"

export default class DailyList extends Component {
    state = {
        name: this.props.data.attend ? this.props.data.attend.trainee : this.props.data.absent.trainee,
        absent: this.props.data.absent ? true : false,
        checkIn: this.props.data.attend ? Object.values(this.props.data.attend)[1].in : null,
        checkOut: this.props.data.attend ? Object.values(this.props.data.attend)[1].out : null,
        date: this.props.data.attend ? Object.keys(this.props.data.attend)[1] : null,
        queryName: null,
        loaded : false,
        reload : false,
        data : null
    }

    studentSelectedHandler = () => {
        localStorage.setItem('StudentName', this.state.name)
    }
    checkInNowHandler = (studentName) => {

        if (studentName.absent) {
            let name = studentName.absent.trainee
            axios.request({
                method: 'patch',
                url: 'http://localhost:5000/att/check_in',
                data: { name }
            })
                .then(({data}) => {
                    this.setState({clicked:true})
                    this.props.setNewState(data)
                    
                })
                .catch(err => {
                    err.response.data.Error ? alert(err.response.data.Error) : alert(err)
                    console.log("error", err)
                })
        }
    }
    checkOutNowHandler = (studentName) => {
        if (studentName.attend) {
            let name = studentName.attend.trainee
            axios.request({
                method: 'put',
                url: 'http://localhost:5000/att/check_out',
                data: { name }
            })
                .then(({data}) => { 
                    // console.log(data)          
                    this.props.setNewState(data)
                    // this.props.componentDidMount()
                })
                .catch(err => {
                    err.response.data.Error ? alert(err.response.data.Error) : alert(err)
                    console.log("error", err)
                })
          
                .then(()=>{
                 
                })
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        return this.state.reload != nextState.reload;
      }

    // componentDidUpdate = ()=>{
    //     console.log('s')

    //     this.setState({
    //         name: this.props.data.attend ? this.props.data.attend.trainee : this.props.data.absent.trainee,
    //         absent: this.props.data.absent ? true : false,
    //         checkIn: this.props.data.attend ? Object.values(this.props.data.attend)[1].in : null,
    //         checkOut: this.props.data.attend ? Object.values(this.props.data.attend)[1].out : null,
    //         date: this.props.data.attend ? Object.keys(this.props.data.attend)[1] : null,
    //        })
    // }
    render () {
        if (!cookie.load('isAuth')) {
            return (
                <div style={{marginTop:'10vh'}}>
                    <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
                </div>
            )
        } else
        return (
            <>
                <tr onClick={() => this.studentSelectedHandler(this.state.data)} style={this.state.absent ? { color: 'red' } : {}}>

                    <td >
                        <Link style={this.state.absent ? { color: 'red' } : {}} className='link' to='/query'>
                            {this.state.name}
                        </Link></td>

                    <td>
                        {this.state.absent ?
                            <Popup trigger={<button style={{ padding: '2px' }} class="button button1">Check-in NOW</button>} position="top center">
                                {this.props.data.absent ? <div style={{ color: 'black', padding: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'center ', alignItems: 'center', alignContent: 'center' }}> <div>Are you sure you want to check in now?</div> <button  onClick={() => this.checkInNowHandler(this.props.data)} style={{ padding: '5px' }} class="button button1">Check in </button></div> : <div style={{ color: 'red', padding: '10px' }}>{`You've already check in`}</div>}
                            </Popup> :
                            <Link className='link' to='/query'>
                                {this.state.checkIn}
                            </Link>}
                    </td>
                    <td >
                        {this.state.checkOut ?
                            <Link style={this.state.absent ? { color: 'red' } : {}} className='link' to='/query'>
                            {this.state.checkOut }
                            </Link>
                            :
                            <Popup trigger={<button style={{ padding: '2px' }} class="button button3">Check-out NOW</button>} position="top center">
                                <div style={{ color: 'black', padding: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'center ', alignItems: 'center', alignContent: 'center' }}> <div>Are you sure you want to check out now?</div> <button  onClick={() => this.checkOutNowHandler(this.props.data)} style={{ padding: '5px' }} class="button button3">Check out </button> </div>
                            </Popup>
                        }
                    </td>
                    <td >
                        <Link style={this.state.absent ? { color: 'red' } : {}} className='link' to='/query'>
                            {this.state.date ? this.state.date : '--------'}
                        </Link>
                    </td>
              
                </tr>


            </>
        )
    }
}

