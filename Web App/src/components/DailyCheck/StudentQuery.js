import React, { Component } from 'react'
import axios from 'axios'
import Modal from 'react-modal';
import ModelEditAttendanceForStudent from "./ModelEditAttendanceForStudent"
import './ButtonStyle.css'
import { Link } from 'react-router-dom'
import cookie from "react-cookies"
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

export default class StudentQuery extends Component {
    state = {
        data: [],
        absentsNumber: 0,
        isVisible : false,
        selectedDate : null
    }
    componentDidMount = () => {
        if (cookie.load('isAuth')) {
      this.fetchStudentQuery()
        }
    }
    fetchStudentQuery=()=>{
        const url = 'https://orange-coding.herokuapp.com/att/student_query'
        axios.get(url, {
            params: {
                name: localStorage.getItem('StudentName')
            }
        })
            .then((res) => {
                let data = Object.keys(res.data)
                let absentsNumber = res.data.absent_num
                this.setState({
                    data,
                    absentsNumber
                })
        
            })
            .catch(err => {
                console.log(err)
                err.response.data.Error ? alert(err.response.data.Error) : alert(err)

            })
    }
    editStudentHandler=(selectedDate)=>{
        this.setState({
            isVisible : true,
            selectedDate
        })
      
    }
    isVisible=(isVisible)=>{
        this.setState({
            isVisible 
        })
    }
    render() {
        if (!cookie.load('isAuth')) {
            return (
                <div style={{marginTop:'10vh'}}>
                    <h2>You don't have permission to access this page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
                </div>
            )
        } else
        return (
            <>        
            <Modal
              isOpen={this.state.isVisible}
              style={customStyles}
              contentLabel="Edit Attendance For Student"
            >
            <ModelEditAttendanceForStudent isVisible={this.isVisible} selectedDate= {this.state.selectedDate} fetchStudentQuery={this.fetchStudentQuery}/>
            </Modal>
                <h2 style={{marginTop:"60px"}}>{`Name: ${localStorage.getItem('StudentName')}`}</h2>
                <h3 style={this.state.data.length > 4 ? {color:'red'} : {}}>{this.state.data.length > 1 ? `This student has been absent for ${this.state.data.length-1} days` :`No absents for this student` }</h3> 
                <div style={{color:'black'}}>
                <ReactHTMLTableToExcel
            
                // id="test-table-xls-button"
                // className="download-table-xls-button"
                
                table="table-to-xls"
                filename={`report for: ${localStorage.getItem('StudentName')}`}
                sheet="tablexls"
                buttonText="Download as Excel file"/>
                </div>
                <div className={'daily-table'}>
                <table id="table-to-xls">
                <div>
                <h2 style={{display:'none'}}>{`Name: ${localStorage.getItem('StudentName')}`}</h2>
                <h3 style={{display:'none'}}>{`This student has  ${this.state.data.length-1}  absent`}</h3> 
                </div>    
                <tr>
                        <th>Dates</th>
                        <th>Edit</th>
                    </tr>
                   
                    {this.state.data.map((date,i) => {
                        return(date != 'absent_num' ? <tr> <td>{date}</td>  <td><button style={{ padding: '0px' }} class="button button1"  onClick={()=>this.editStudentHandler(date)}>Edit</button></td></tr> : '')
                    })}
                </table>
                </div>
            </>
        )
    }
}
const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '50%',
      width                 : '35rem',
    //   zIndex: 1,
      transform             : 'translate(-50%, -50%)'
    }
  };