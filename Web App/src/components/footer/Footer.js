import React from 'react'
import './footer.css'
export default ()=>{
    return (
      <div className={'footerContainer'}>
      <ul className={'footerUl'}>

      <li className={'footerLi'}><a class={'footerA'} href="tel:+1(949)2996669">© 2020 Asem Qaffaf</a></li>
        <li className={'footerLi'}><a class={'footerA'} href="https://github.com/asemqaffaf" target="_blank">Github</a></li>
        <li className={'footerLi'}><a class={'footerA'} href="https://expo.io/@asemqaffaf" target="_blank">Expo</a></li>
        <li className={'footerLi'}><a class={'footerA'} href="mailto:asem@qaffaf.com" target="_blank">Email</a></li>
        <li className={'footerLi'}><a class={'footerA'} href="https://www.linkedin.com/in/asem-qaffaf-75b541199/" target="_blank">LinkedIn</a></li>
        <li className={'footerLi'}>
        <p className={'footerP'}>👋</p>
        </li>
        </ul>
    </div>
    )
}