import React, { Component } from 'react'
import 'moment/locale/en-gb.js'
import {  DatePickerInput } from 'rc-datepicker';
import "./../Style/App.css"
import 'rc-datepicker/lib/style.css';
import cookie from "react-cookies"
import { Link } from "react-router-dom"
import moment from "moment"
import axios from 'axios'
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class Calender extends Component {
    // Saturday:28-12-2019
    state = {
        day: moment().format("DD"),
        month: moment().format("MM"),
        year: moment().format("YYYY"),
        dayStr: moment().format('dddd'),
        data: {}
    }
    date = `${this.state.year + "-" + this.state.month + "-" + this.state.day}`

    onChange = (jsDate, dateString) => {
        let StartDate = moment(jsDate).format('dddd');
        try {
            var strYear = dateString.split("-")[0]
            var strMonth = dateString.split("-")[1]
            var strDay = dateString.split("-")[2]
            var dayStr = StartDate
            strDay = strDay.split("T")[0]
            this.setState({
                day: strDay,
                month: strMonth,
                year: strYear,
                dayStr: dayStr
            })
        }
        catch{

            alert("Please select a valid date!")
        }

    }
    // componentDidMount = () => {
    // }
    fetchDateData = async() => {
        const userPrivileges = await cookie.load('user_Privileges')
        const date = `${this.state.dayStr}:${this.state.day + "-" + this.state.month + "-" + this.state.year}`
        const url = 'https://orange-coding.herokuapp.com/att/search_by_date'
        axios.request({
            url,
            method: 'GET',
            params: { date },
            auth: {
                username: "admin",
                password: userPrivileges
              }
        }).then(({ data }) => {
            // console.log(data)
            this.setState({
                data
            })

        })
            .catch(error => {
                console.log(error)
                error.response.data.Error ? alert(error.response.data.Error) : alert(error)
            })

    }
    studentSelectedHandler = (student) => {
        localStorage.setItem('StudentName', student)
    }
    render() {
        if (!cookie.load('isAuth')) {
            return (
                <div style={{marginTop:'10vh'}}>
                    <h2>You don't have permission to access History page <br></br> <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link></h2>
                </div>
            )
        } else
            return (
                <React.Fragment>
                    <h1>History Page</h1>
                    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                        <DatePickerInput style={{ width: 50 + 'vw' }}

                            onChange={this.onChange}
                            value={this.date}
                        />
                        <button style={{ padding: '10px' }} className={"button button1"} onClick={this.fetchDateData}>Submit</button>
                        { Object.entries(this.state.data).length == 0 ? <div></div> : 
                            <div style={{color:'black'}}>
                            <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="download-table-xls-button"
                        table="table-to-xls"
                        filename={`${this.state.dayStr}:${this.state.day + "-" + this.state.month + "-" + this.state.year}`}
                        sheet="tablexls"
                        buttonText="Download as Excel file"/> </div>}
                        
                        <table id="table-to-xls">
                            <tr>
                                <th>Name <span style={{ fontSize: '15px', color: 'tomato' }}> click on any name to see their history</span></th>
                                <th>Check in</th>
                                <th>Check out</th>
                            </tr>
                            {Object.keys(this.state.data).map(students => {
                                return (<tr onClick={() => this.studentSelectedHandler(students)}>   {this.state.data[students].in ?  <td > <Link style={{color:'white'}} to='/query'> {students}</Link> </td> : <td style={{ color: 'red' }}>  <Link style={{color:'red'}} to='/query'>{students} </Link></td>}
                                    {this.state.data[students].in ? <td> <Link style={{color:'white'}} to='/query'> {this.state.data[students].in}</Link></td> : <td style={{ color: 'red' }}> <Link style={{color:'red'}} to='/query'> absent </Link></td>}
                                    {this.state.data[students].out ? <td> <Link style={{color:'white'}} to='/query'> {this.state.data[students].out}</Link></td> : <td ><Link style={{color:'red'}} to='/query'>  ---- </Link></td>}
                              </tr>        )
                            })}
                        </table>
                    </div>
                </React.Fragment>
            )
    }
}

export default Calender;