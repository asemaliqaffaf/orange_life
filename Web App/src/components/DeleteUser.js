import React, { Component } from 'react'
import axios from 'axios'
import cookie from "react-cookies"
import { Link } from "react-router-dom"
import Popup from "reactjs-popup";

export default class DeleteUser extends Component {
    state = {
        data: [],
        nameToDelete: null,
        deleteFirst: false
    }
    componentDidMount() {
        this.fetchTraineeList()
    }
    fetchTraineeList = async () => {
        const userPrivileges = await cookie.load('user_Privileges')
        let url = 'https://orange-coding.herokuapp.com/att/students_list'
        axios.request({
            url,
            method: 'GET',
            auth: {
                username: "admin",
                password: userPrivileges
            }
        })
            .then(({ data }) => {

                this.setState({
                    data
                })
            })
            .catch(error => {
                error.response.data.Error ? alert(error.response.data.Error) : alert(error)
            })
    }
    deleteUserHandler = async(name) => {
        const userPrivileges = await cookie.load('user_Privileges')
        const data = { name }

        const url = 'https://orange-coding.herokuapp.com/usr/delete_user_name'
        axios.request({
            url,
            method: 'DELETE',
            data,
            auth: {
                username: "admin",
                password: userPrivileges
            }
        }).then(({ data }) => {
            this.setState({
                deleteFirst: data
            })
        })
            .catch(err => {
                err.response.data.Error ? alert(err.response.data.Error) : alert(err)
                alert(err)
            })
            .then(async () => {
                const userPrivileges = await cookie.load('user_Privileges')
                const url = 'https://orange-coding.herokuapp.com/att/delete_trainee'
                axios.request({
                    url,
                    method: 'DELETE',
                    data,
                    auth: {
                        username: "admin",
                        password: userPrivileges
                    }
                })
                    .then(({ data }) => {
                        if (this.state.deleteFirst && data) {
                            this.fetchTraineeList()
                        }
                    })
                    .catch(err => {
                        err.response.data.Error ? alert(err.response.data.Error) : alert(err)

                        alert(err)
                    })
            })
    }
    render() {
        if (cookie.load('user_Privileges') == null) {
            return (
                <div style={{ marginTop: '10vh' }}>
                    <h2>You don't have permission to access Register page <br></br>
                        {cookie.load('isAuth') ? <h2>You are not Authorized to access this page!</h2> : <Link style={{ color: 'lightblue' }} to="/" >Please sign in </Link>}
                    </h2>
                </div>
            )
        } else
            return (
                <>
                    <h1>Delete User</h1>

                    <table>
                        <tr>
                            <th> Name: <span style={{ fontSize: '15px', color: 'red' }}>Warning! you will not be able to retrieve this student</span></th>
                            <th>Deletion</th>
                        </tr>

                        {this.state.data.map((name, i) => {
                            return (<tr> <td key={i}>{name}</td>
                                <td>
                                    <Popup trigger={<button style={{ padding: '2px' }} class="button button3">Delete</button>} position="left center">
                                        <div style={{ color: 'black', padding: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'center ', alignItems: 'center', alignContent: 'center' }}> <div style={{ color: 'red' }} >Are you sure you want to delete this user?</div><div style={{ fontSize: 15 }}>Please enter the person who you want to delete</div> </div> {this.state.nameToDelete == name ? <button disable={true} onClick={() => this.deleteUserHandler(name)} class="button button3">{`DELETE`}</button> : <input onChange={(event) => this.setState({ nameToDelete: event.target.value })}></input>}  }
                                </Popup>
                                </td>
                            </tr>)
                        })}

                    </table>



                </>
            )
    }
}
