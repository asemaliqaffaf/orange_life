/*  _                          ___         __  __        __ 
   / \   ___  ___ _ __ ___    / _ \  __ _ / _|/ _| __ _ / _|
  / _ \ / __|/ _ \ '_ ` _ \  | | | |/ _` | |_| |_ / _` | |_ 
 / ___ \\__ \  __/ | | | | | | |_| | (_| |  _|  _| (_| |  _|
/_/   \_\___/\___|_| |_| |_|  \__\_\\__,_|_| |_|  \__,_|_|  
*/
const express = require('express');
const favicon = require('express-favicon');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();
app.use(favicon(__dirname + '/build/favicon.ico'));
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'build')));
var request = require('request');
app.get('/api',  (req, res)=> {
  var api = 'https://orange-coding.herokuapp.com';
  request(api).pipe(res);});
app.get('/*',  (req, res)=> {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
app.listen(port,()=>console.log(`Front End works at ${port}`));