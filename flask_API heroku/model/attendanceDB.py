from .database import db
from .output import return_function
trainee = db.trainee
db.list_collection_names()
[u'trainee']
import threading

from datetime import datetime
import pytz

# Amman current date-and-time
def local_date_time():
    date = datetime.now(pytz.timezone('Asia/Amman')) 
    return date

def date_now():
    date = local_date_time()
    date_now = str(f'{date.strftime("%A")}:{date.strftime("%d")}-{date.strftime("%m")}-{date.strftime("%Y")}')
    return date_now

def day_str():
    date = local_date_time()
    day_str = date.strftime("%A")
    return day_str
def time_now():
    date = local_date_time()
    time_now = str(f'{date.strftime("%H")}:{date.strftime("%M")}')
    return time_now

# 1. get all trainees data. @Admin
def get_all_data():
    all_data = trainee.find()
    return return_function(all_data)

# 2. add new student. @Admin
def add_new_student(name):
    new_student = {'trainee' : name}
    if validation_checker(new_student):
        trainee_id = trainee.insert(new_student)
        add_new_student_to_list = {'trainee' : 'trainee_list'}
        if validation_checker(add_new_student_to_list):
            trainee.insert(add_new_student_to_list)
        trainee_list = trainee.update_one(add_new_student_to_list , { '$set': { name : 0  } })
    else:
        return {'Error' : 'you already have this student in your list'} , 409
    return return_function(new_student)


# 3. add current data beside trainee name 
#    and add in time inside it.
def check_in(name , time = '__None__' , date = '__None__'):
    if time == '__None__':
        time = time_now()
    if date == '__None__':
        date = date_now()
    student_in = {'trainee' : name}
    if validation_checker(student_in) is False:
        modified_student = trainee.update_one(student_in , {'$set': {date : {'in': time}}}, upsert=True)
    else:
        return {'Error' :'no user found please add new student'} , 409
    # return return_function(modified_student.acknowledged) , 201
    return students_daily_query()


# 4. add out time inside current date.
def check_out(name , time = '__None__' , date = '__None__'):
    if time == '__None__':
        time = time_now()
    if date == '__None__':
        date = date_now()
    student_out = {'trainee' : name}
    trainee_object = trainee.find(student_out)
    try:
        time_checked_in = str(trainee_object[0][date]['in'])
        modified_student = trainee.update_one(student_out , {'$set': {date : {'in': time_checked_in ,'out': time}}}, upsert=True)
    except:
       return {'Error' :"error check_in first"} ,  409
    # return return_function(modified_student.acknowledged) ,  201
    return students_daily_query()

###--VIEW--###
# 5. check who attend or not daily query
def students_daily_query():
    # trainee_list_query = {'trainee' : 'trainee_list'}
    # std_list_arr = trainee.find(trainee_list_query)
    # std_list =  std_list_arr[0].keys() # list all students in one object
    # print(date_now().split(":")[0])
    # Friday Saturday
    if date_now().split(":")[0] != 'Friday' and date_now().split(":")[0] != 'Saturday':
        students_profiles = trainee.find()  # the WHOLE database all objects in one array
        date = date_now()
        output = []
        for std in students_profiles:
            # for std_lst in std_list:
                if std['trainee'] != 'trainee_list':
                    if date in std and 'in' in std[date]:
                        output.append({'attend':  {'trainee':std["trainee"] , date: std[date] }}) # students have been attended
                    else:
                        trainee.update_one({'trainee': std["trainee"]} , {'$set': {date : f'absent'}}, upsert=True)
                        output.append({'absent': {'trainee':std["trainee"]}}) # students have been absent
        return return_function(output)
    else:
        return return_function([])
# 6. check how many time the student has been absent @Admin
def student_query(name):
    std_want_query = {"trainee": name}
    if validation_checker(std_want_query) is False:
            std_profile = trainee.find(std_want_query)[0]
            std_key = std_profile.keys()
            std_list = trainee.find({'trainee' : 'trainee_list'})
            absent_counter = std_list[0][name]
            output = {}
            output['absent_num'] =  absent_counter 
            counter = 0
            for key in std_key:
                if key != 'trainee' and key != '_id' and 'absent' in std_profile[key]:
                    counter +=1
                    output[key] = std_profile[key]
            trainee.update_one({'trainee' : 'trainee_list'}, { '$set': { name : counter  } }, upsert=True)  
            return return_function(output) , 200
    else:
        return "check the name that you've entered " , 400

# 7. students list @Admin
def students_list():
    trainee_list_query = {'trainee' : 'trainee_list'}
    std_list_arr = trainee.find(trainee_list_query)
    std_list =  std_list_arr[0].keys()
    final_list = []
    for std in std_list:
        if std != '_id' and std != 'trainee':
            final_list.append(std)
    return return_function(final_list)

# 8 full report for a student @Admin
def full_report(name):
    trainee_list_query = {'trainee' : name}
    std_report = trainee.find(trainee_list_query)
    if std_report.count() == 0:
        return return_function({'Error' : 'no data found'}), 409
    else: 
        return return_function(std_report) , 200

# 9 search by date @Admin
def search_by_date(date):
    list_in_that_time = trainee.find({ date :  { '$exists' : True } })
    if list_in_that_time.count() != 0:
        obj_in_that_date = {}
        for name in list_in_that_time:
            obj_in_that_date[name['trainee']] = name[date]
        return return_function(obj_in_that_date) , 200
    else:
        return return_function({'Error': 'No Data Found' }), 409
# 10. delete user name (name) by giving their name
def delete_trainee(name):
    trainee_list = trainee.update_one({'trainee' : 'trainee_list'} ,{ '$unset' : { name :{ '$exists' : True }  } })
    trainee_object = {'trainee' : name}
    trainee_document = trainee.delete_one(trainee_object)
    return return_function(True)

# 12. Duration Query (phase two)
def duration_query(name,duration_array ):
        trainee_list_query = {'trainee' : name}
        std_report = trainee.find(trainee_list_query)
        arr = []
        for date in std_report:
            for duration in duration_array:
                try :
                    arr.append({duration : date[duration]})
                    print({duration : date[duration]})
                except :
                 print('error!')
              #    pass
        print(arr)
        return return_function(arr)

# 13 Delete attendance date (phase two)
def delete_attendance_date(name,date):
    std_report = trainee.find( {'trainee' : name})
    if std_report.count() > 0:
        try:
            trainee.update_one({'trainee': name } , {'$set': {date : f'absent'}}, upsert=True)
            return return_function(std_report)
        except:
           return_function({'Error': 'No Data Found' }), 409
    return return_function({'Error': 'No Data Found' }), 409
'''
================================================================================================================================
'''
# trainee and their name validation
def validation_checker(trainee_info):
    trainee_object = trainee.find(trainee_info)
    if (trainee_object.count() == 0):
        return True
    else:
        return False