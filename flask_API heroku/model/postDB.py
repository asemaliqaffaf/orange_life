from .database import db
from .output import return_function
from bson import ObjectId

posts = db.posts
db.list_collection_names()
[u'posts']

# 1. upload file to the server
# POST form-data => image : 'image.jpg'
# GET body : 'text'
def post(file_name, body_text, name):
   post = file_name
   post['body'] = body_text
   post['name'] = name
   data = posts.insert(post)
   return return_function( post)

# 2. view_one no needed database


# 3. view all posts from database note: filename contains file name only you have to use view_one in order to view it
def view_all():
   all_posts = posts.find()
   return return_function(all_posts)

# 4. delete file from server

# 5. delete file data from database such as file name who make the post
def delete_file_path_db(post_id):
   result = posts.delete_one({'_id': ObjectId(post_id)})
   return return_function( True)