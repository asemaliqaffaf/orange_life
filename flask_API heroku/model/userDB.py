from controller.Hash_password_generator import User
from .database import db
from .output import return_function
import os
import sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
users = db.users
db.list_collection_names()
[u'users']

# 1. create new user


def new_user(name, email, password, national_id, is_admin=False):
    if is_admin == False:
        hashing_is_admin = User()
        hashing_is_admin = hashing_is_admin.hash_password(str(is_admin))
        is_admin = hashing_is_admin
    new_added_user = {
        'name': name,
        'email': email,
        'national_id': national_id,
        "password": password,
        'admin_user': is_admin
    }
    validation = validation_checker({'email': email})
    if validation:
        added_user = users.insert(new_added_user, {'unique': True})
        validate_is_admin = User()
        validate_is_admin = validate_is_admin.verify_password(
            'True', is_admin)  # :-)
        if validate_is_admin == True:
            return return_function({'name': new_added_user['name'], 'email': new_added_user['email'],'national_id': new_added_user['national_id'] , 'admin_user': new_added_user['admin_user']}), 201
        return return_function({'name': new_added_user['name'],'national_id': new_added_user['national_id'] ,'email': new_added_user['email']}), 201
    else:
        return return_function({'Error': 'this email address is already in use'}), 409

# 2. check auth for email and password


def user_auth(email, password, is_admin=False):
    user_entered = {
        'email': email
    }
    validation = users.find(user_entered)
    verified_password = False
    if(validation.count() == 1):
        hashed_password = validation[0]['password']
        hashed_admin_role = is_admin
        try:
            verified_password = User()
            verified_password = verified_password.verify_password(
                password, hashed_password)
        except:
            verified_password = False
    if verified_password == True:
        try:
            verified_admin_role = User()
            verified_admin_role = verified_admin_role.verify_password(
                'True', validation[0]['admin_user'])  # :-)
            if verified_admin_role == True:
                return return_function({'name': validation[0]['name'], 'email': validation[0]['email'], 'admin_user': validation[0]['admin_user']}), 200
            else:
                return return_function({'name': validation[0]['name'], 'email': validation[0]['email']}), 200
        except:
            return return_function({'Error': 'Unauthorized Access data !'}), 409
    else:
        return return_function({'Error': 'check your email or password'}), 409

# 3. change password for particular user


def change_password(email, password):
    email_to_change_pass = {'email': email}
    user_info = users.update(email_to_change_pass, {
                             '$set': {"password": password}})
    if user_info['nModified'] == 1:
        return return_function({'email': email}), 201
    else:
        return return_function({"Error": "something went wrong"}), 409


# 4. delete user name (email) by giving a name
def delete_user_name(name):
    name_for_deletion = {'name': name}
    delete_result = users.delete_one(name_for_deletion)
    return return_function(True)


def test_get_user(user):
    # users.find(user)
    return user


# username or username-and-password validation
def validation_checker(user_name):
    user_info = users.find(user_name)
    if (user_info.count() == 0):
        return True
    else:
        return False
