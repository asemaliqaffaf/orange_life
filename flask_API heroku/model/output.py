from flask import Response 
from bson import json_util


# this function to change bson files to be returnable.
# also return them as json file to the api
def return_function(bson_file):
    return Response(
        json_util.dumps(bson_file),
        mimetype='application/json'
    )