from flask import Flask ,Blueprint , request
import  model.attendanceDB  as db
import json
from flask_httpauth import HTTPBasicAuth
app = Flask(__name__)
# app.config[' d'] = True
app = Blueprint('attendance', __name__, template_folder='templates')
auth = HTTPBasicAuth()
from .Hash_password_generator import User
@auth.verify_password
def verify_password(user_name, password):
    verify_password_from_user =  User()
    verify_password_from_user.verify_password('True',password)
    return verify_password_from_user

# 1. get all trainees data. @Admin
@app.route('/att/data', methods=['GET'] )
@auth.login_required
def data():
    return db.get_all_data()
    # return db.return_function( data)

# 2. add new student. @Admin
@app.route('/att/add_new' , methods=['POST'])
@auth.login_required
def add_new_student():
    if 'name' in request.json:   
        trainee_name = request.json['name'].lower()
        return db.add_new_student(trainee_name)
    else:
        return 'bad request' , 400

# 3. add current data beside trainee name 
#    and add in time inside it.
@app.route('/att/check_in' , methods=['PATCH'])
def check_in():
    if 'name' in request.json:
        trainee_name = request.json['name'].lower()
        if 'time' in request.json and 'date' in request.json:
            time_to_check_in = request.json['time']
            date_to_check_out = request.json['date']
            return db.check_in(trainee_name , time_to_check_in, date_to_check_out)
        else:
            return db.check_in(trainee_name)
    else:
        return 'bad request' , 400

# 4. add out time inside current date.
@app.route('/att/check_out' , methods=['PUT'])
def check_out():
    if 'name' in request.json:   
        trainee_name = request.json['name'].lower()
        if 'time' in request.json and 'date' in request.json:
            time_to_check_out = request.json['time']
            date_to_check_out = request.json['date']
            return db.check_out(trainee_name , time_to_check_out , date_to_check_out)
        else:
            return db.check_out(trainee_name)
    else:
        return 'bad request' , 400



# 5. check who attend or not daily query @Admin
@app.route('/att/students_daily_query' , methods=['GET']) # here ! login_required  
@auth.login_required 
def students_daily_query():
    try:
        return db.students_daily_query(), 200
    except:
        return "bad request" , 400

# 6. check how many time the student had been absent @Admin
@app.route('/att/student_query' , methods=['GET'])
def student_query():
    try:
        if 'name' in  request.args:
            return db.student_query(request.args['name'].lower()) 
        else:
            return "bad request" , 400
    except:
            return "bad request" , 400

# 7. students list @Admin
@app.route('/att/students_list' , methods=['GET'])
@auth.login_required 
def students_list():
    try :
        return db.students_list() , 200
    except:
        return {'Error' : 'no connections'} , 404

# 8 full report for a student @Admin
@app.route('/att/full_report' , methods=['POST'])
@auth.login_required 
def full_report():
    if 'name' in request.json:
        name = request.json['name'].lower()
        return db.full_report(name)

# 9 search by date @Admin
@app.route('/att/search_by_date', methods=['GET'])
@auth.login_required 
def search_by_date():
    if 'date' in request.args:
        return db.search_by_date(request.args['date'])
    else:
        return 'bad request' , 409
    
# 10. delete user name (name) by giving their name
@app.route('/att/delete_trainee' , methods=['DELETE'])
@auth.login_required 
def delete_trainee():
    if 'name' in request.json:
        name = request.json['name']
        return db.delete_trainee(name) , 201
    else:
        return 'bad request' , 401
import uuid

# 11. sync QR value
@app.route('/att/sync_qr', methods=['GET'])
def sync_qr():
    return str(uuid.uuid4())

# 12. Duration Query (phase two)
@app.route('/att/duration_query',methods=['POST'])
@auth.login_required 
def duration_query():
    try:
         if 'name' in request.json and 'duration_array' in request.json :
            name = request.json['name'].lower()
            duration_array = request.json['duration_array']
            # print(duration_dates_array)
            return db.duration_query(name,duration_array)
    except:
        return 'bad request' , 401

# 13 Delete attendance date (phase two)
@app.route('/att/delete_attendance_date',methods=['DELETE'])
@auth.login_required
def delete_attendance_date():
    if 'name' in request.json and 'date' in request.json:
        name = request.json['name'].lower()
        date = request.json['date']
        return db.delete_attendance_date(name,date)