import model.postDB as db

# 1. upload file to the server
# POST form-data => image : 'image.jpg'
# GET body : 'text'
def path_to_db(file_name, body_text , name ):
    return db.post(file_name,body_text , name)

# 2. view_one no needed database

# 3. view all posts from database note: filename contains file name only you have to use view_one in order to view it
def view_all():
    return db.view_all()

# 4. delete file from server

# 5. delete file data from database such as file name who make the post
def delete_file_path_db(post_id):
    return db.delete_file_path_db(post_id)