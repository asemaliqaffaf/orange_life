from passlib.hash import pbkdf2_sha256

class User:
    def __init__(self):
        pass
    def hash_password(self, password):
        pwd_hash = pbkdf2_sha256.hash(password)
        return pwd_hash

    def verify_password(self, password, hash):
        return pbkdf2_sha256.verify(password, hash)