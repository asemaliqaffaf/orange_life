from flask import Flask , request , jsonify ,Response , Blueprint
app = Flask(__name__)
# app.config['DEBUG'] = True
app = Blueprint('user', __name__, template_folder='templates')
import model.userDB  as db
import json
from flask_httpauth import HTTPBasicAuth
from validate_email import validate_email
from .Hash_password_generator import User
auth = HTTPBasicAuth()
@auth.verify_password
def verify_password(user_name, password):
    verify_password_from_user =  User()
    verify_password_from_user.verify_password('True',password)
    return verify_password_from_user

# 1. create new user @Admin
@app.route('/usr/newuser' , methods=['POST'])
@auth.login_required
def new_user_app():
    if 'email' in request.json and 'password' in request.json and 'name' in request.json and 'national_id' in request.json:
        if is_valid(request.json['email']):
            name = request.json['name'].lower()
            hashing_password = User()
            hashing_password = hashing_password.hash_password(request.json['password'])
            if 'admin_user' in request.json and request.json['admin_user'] == True:
                hasing_is_admin = User()
                hashing_is_admin = hasing_is_admin.hash_password(str(request.json['admin_user']))
                return db.new_user(name=name ,email= request.json['email'].lower() ,password= hashing_password ,national_id= request.json['national_id'],is_admin= hashing_is_admin )
            return db.new_user(name= name,email= request.json['email'].lower() ,national_id= request.json['national_id'], password= hashing_password)
        else:
            return db.return_function('enter valid email') , 400
    else:
        return 'bad request' , 400

# 2. check auth for email and password
@app.route('/usr/auth', methods=['POST'])
def auth_user_and_password():
    if 'email' in request.json and 'password' in request.json:   
        if is_valid(request.json['email']):
            # if 'admin_user' in request.json:
            #     return db.user_auth(request.json['email'].lower() , request.json['password'] , request.json['admin_user'])
            return db.user_auth(request.json['email'].lower() , request.json['password'])
        else:
          return db.return_function('enter valid email') , 400
    else:
        return 'bad request' , 400

# 3. change password for particular user
@app.route('/usr/change_password' , methods=['PUT'])
@auth.login_required
def change_password():
    if 'email' in request.json and 'password' in request.json:
        if is_valid(request.json['email']):
            hashing_password = User()
            hashing_password = hashing_password.hash_password(request.json['password'])  
            return db.change_password(request.json['email'] , hashing_password)
        else:
            return db.return_function('enter valid email') , 400
    else:
        return 'bad request' , 400

# 4. delete user name (email) by giving a name
@app.route('/usr/delete_user_name' , methods=['DELETE'])
@auth.login_required
def delete_user_name():
    if 'name' in request.json:
        name = request.json['name']
        return db.delete_user_name(name) , 201
    else:
        return 'bad request' , 401
@app.route('/test' , methods=['GET'])
def test():
    return db.test_get_user(request.args['name'])
# email validation
def is_valid(email):
    return validate_email(email) # regx