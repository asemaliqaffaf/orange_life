""" _                          ___         __  __        __ 
   / \   ___  ___ _ __ ___    / _ \  __ _ / _|/ _| __ _ / _|
  / _ \ / __|/ _ \ '_ ` _ \  | | | |/ _` | |_| |_ / _` | |_ 
 / ___ \\__ \  __/ | | | | | | |_| | (_| |  _|  _| (_| |  _|
/_/   \_\___/\___|_| |_| |_|  \__\_\\__,_|_| |_|  \__,_|_|            
"""
import controller.users as user
import controller.attendance as attendance
import controller.post as post
from flask import Flask ,Blueprint  , request , send_file
from flask_cors import CORS
import json
import os, sys
import uuid
# from gevent.pywsgi import WSGIServer

app = Flask(__name__)
CORS(app)
app.config['DEBUG'] = True

@app.route('/',  methods=["GET"])
def home():
    str = "<center><h1>Welcome to Orange Coding Life API</h1>"
    str += "<br><h2>Created by Asem Qaffaf</h2>"
    str += '<br><a href="https://github.com/asemqaffaf" >github.com/asemqaffaf</a></center>'
    return str

PROJECT_HOME = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = '{}/uploads/'.format(PROJECT_HOME)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# 1. upload file to the server
# POST using form-data => image : 'image.jpg'
# GET using  query string => body : 'text'
@app.route('/file/upload' , methods=['GET' , 'POST'])
def upload():
    if request.method == 'POST':
        if  'body' in request.args and 'name' in request.args:
            file = request.files['image']
            body_text = request.args['body']
            name = request.args['name']
            extension = os.path.splitext(file.filename)[1]
            f_name = str(uuid.uuid4()) + extension
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], f_name))
            return post.path_to_db({'filename':f_name}, body_text , name) , 201
        else:
            return ('bad request') , 401

# 2. view one using GET with query string => file_name : 'image.jpeg' note: this method is only for view no needed database
@app.route('/file/view_one' , methods=['GET'])
def view_on():
    file_name = request.args['file_name']
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'],  file_name)
    return  send_file(full_filename , mimetype='image/jpg')
# 3. view all posts from database note: filename contains file name only you have to use view_one in order to view it
@app.route('/file/view_all' , methods=['GET'])
def view_all():
    return post.view_all()

# 4. delete file from server
@app.route('/file/delete_file', methods=['GET', 'POST'])
def delete_item():
    if 'file_name' in request.json:
        file_name =  request.json['file_name']
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'],  file_name))
        return 'done' , 201
    else:
        return 'bad request' , 401
        
# 5. delete file data from database such as file name who make the post
@app.route('/file/delete_file_path_db' , methods=['DELETE'])
def delete_file_path_db():
    if 'post_id' in request.json:
        post_id = request.json['post_id']
        return post.delete_file_path_db(post_id) , 201
    else:
        return 'bad request' , 401



app.register_blueprint(attendance.app)
app.register_blueprint(user.app)

from gevent.pywsgi import WSGIServer

if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5000)
    # http_server = WSGIServer(('', 5000), app)
    # http_server.serve_forever()

